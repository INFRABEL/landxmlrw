﻿namespace LANDXMLRW.Enumerations.Units
{
    /// <summary>
    /// angular values expressed in "decimal dd.mm.ss" units have the numeric
    ///format "45.3025" representing 45 degrees 30 minutes and 25 seconds. Both the minutes and seconds must be two characters with a numeric range between 00 to 60.
    /// </summary>
    public static  class AngularType
    {
        public static string Radians = "radians";
        public static string Grads = "grads";
        public static string DecimalDegrees = "decimal degrees";
        public static string DecimalDdmmss = "decimal dd.mm.ss";
    }
}
