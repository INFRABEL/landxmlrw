﻿using System.Collections.Generic;

namespace LANDXMLRW.Model.Feature
{
    public class FeatureElement : IFeatureElement
    {
        private IList<IFeatureElement> _features = new List<IFeatureElement>(); 
        public IList<IFeatureElement> Features
        {
            get { return _features; }
            set
            {
                _features = value;
            }
        }

        private IList<IDocFileRefElement> _docFileRefElements = new List<IDocFileRefElement>(); 
        public IList<IDocFileRefElement> DocFileReferences
        {
            get
            {
                return _docFileRefElements;
            }
            set
            {
                _docFileRefElements = value;
            }
        }

        private IList<IPropertyElement> _properties = new List<IPropertyElement>();
        public IList<IPropertyElement> Properties
        {
            get
            {
                return _properties;
            }
            set
            {
                _properties = value;
            }
        }

        public string Name { get; set; }

        public string Code { get; set; }

        public string Source { get; set; }
    }
}
