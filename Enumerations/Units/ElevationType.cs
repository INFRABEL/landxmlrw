﻿namespace LANDXMLRW.Enumerations.Units
{
    /// <summary>
    /// Represents the elevation unit for elevation attribute values, such as ellipsoidHeight
    /// </summary>
    public enum ElevationType
    {
        // ReSharper disable InconsistentNaming
        meter,
        kilometer,
        feet,
        miles
        // ReSharper restore InconsistentNaming
    }
}
