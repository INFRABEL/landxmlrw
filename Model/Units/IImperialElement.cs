﻿using LANDXMLRW.Enumerations.Units;
using LANDXMLRW.Enumerations.Units.Imperial;

namespace LANDXMLRW.Model.Units
{
    public interface IImperialElement : IUnitElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Imperial">
        //  <xs:annotation>
        //    <xs:documentation/>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:attribute name="areaUnit" type="impArea" use="required"/>
        //    <xs:attribute name="linearUnit" type="impLinear" use="required"/>
        //    <xs:attribute name="volumeUnit" type="impVolume" use="required"/>
        //    <xs:attribute name="temperatureUnit" type="impTemperature" use="required"/>
        //    <xs:attribute name="pressureUnit" type="impPressure" use="required"/>
        //    <xs:attribute name="diameterUnit" type="impDiameter"/>
        //    <xs:attribute name="widthUnit" type="impWidth"/>
        //    <xs:attribute name="heightUnit" type="impHeight"/>
        //    <xs:attribute name="velocityUnit" type="impVelocity"/>
        //    <xs:attribute name="flowUnit" type="impFlow"/>
        //    <xs:attribute name="angularUnit" type="angularType" default="radians"/>
        //    <xs:attribute name="directionUnit" type="angularType" default="radians"/>
        //    <xs:attribute name="latLongAngularUnit" type="latLongAngularType" default="decimal degrees"/>
        //    <xs:attribute name="elevationUnit" type="elevationType" default="meter"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        ImperialArea AreaUnit { get; set; }
        ImperialLinear LinearUnit { get; set; }
        ImperialVolume VolumeUnit { get; set; }
        ImperialTemperature TemperatureUnit { get; set; }
        ImperialPressure PressureUnit { get; set; }
        #endregion

        #region Optional
        ImperialDiameter? DiameterUnit { get; set; }
        ImperialWidth? WidthUnit { get; set; }
        ImperialHeigth? HeightUnit { get; set; }
        ImperialVelocity? VelocityUnit { get; set; }
        ImperialFlow? FlowUnit { get; set; }
        string AngularUnit { get; set; }
        string DirectionUnit { get; set; }
        string LatLongAngularUnit { get; set; }
        ElevationType? ElevationUnit { get; set; }
        #endregion
    }
}
