﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model
{
    /// <summary>
    /// 
    /// </summary>
    public interface IProjectElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Project">
        //  <xs:complexType>
        //    <xs:choice minOccurs="0" maxOccurs="unbounded">
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:any namespace="##other" processContents="skip" minOccurs="0"/>
        //    </xs:choice>
        //    <xs:attribute name="name" type="xs:string" use="required"/>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="state" type="stateType"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }
        #endregion

        #region Optional
        /// <summary>
        /// 
        /// </summary>
        StateType? State { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Description { get; set; }

        IList<IFeatureElement> Features { get; set; } 
        #endregion
    }
}
