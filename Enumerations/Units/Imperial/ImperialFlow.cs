﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialFlow
    {
        // ReSharper disable InconsistentNaming
        US_gallonPerDay,
        IMP_gallonPerDay,
        cubicFeetDay,
        US_gallonPerMinute,
        IMP_gallonPerMinute,
        acreFeetDay,
        cubicFeetSecond
        // ReSharper restore InconsistentNaming
    }
}
