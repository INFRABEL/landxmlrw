﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricWidth
    {
        // ReSharper disable InconsistentNaming
        millimeter,
        centimeter,
        meter,
        kilometer
        // ReSharper restore InconsistentNaming
    }
}
