﻿using System;
using System.Collections.Generic;
using LANDXMLRW.Model.Alignment;
using LANDXMLRW.Model.Units;

namespace LANDXMLRW.Model
{
    public interface ILandXmlElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="LandXML">
        //  <xs:complexType>
        //    <xs:choice maxOccurs="unbounded">
        //      <xs:element ref="Units"/>
        //      <xs:element ref="CoordinateSystem" minOccurs="0"/>
        //      <xs:element ref="Project" minOccurs="0"/>
        //      <xs:element ref="Application" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Alignments" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="CgPoints" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Amendment" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="GradeModel" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Monuments" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Parcels" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="PlanFeatures" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="PipeNetworks" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Roadways" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Surfaces" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Survey" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="FeatureDictionary" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:any namespace="##other" processContents="skip" minOccurs="0"/>
        //    </xs:choice>
        //    <xs:attribute name="date" type="xs:date" use="required"/>
        //    <xs:attribute name="time" type="xs:time" use="required"/>
        //    <xs:attribute name="version" type="xs:string" use="required"/>
        //    <xs:attribute name="language" type="xs:string"/>
        //    <xs:attribute name="readOnly" type="xs:boolean"/>
        //    <xs:attribute name="LandXMLId" type="xs:int"/>
        //    <xs:attribute name="crc" type="xs:integer"/>
        //  </xs:complexType>
        //  <xs:unique name="uCoordGeomName">
        //    <xs:selector xpath="CoordGeom"/>
        //    <xs:field xpath="@name"/>
        //  </xs:unique>
        //  <xs:unique name="uCgPointsName">
        //    <xs:selector xpath="CgPoints"/>
        //    <xs:field xpath="@name"/>
        //  </xs:unique>
        //  <xs:unique name="uRoadwayName">
        //    <xs:selector xpath="Roadways/Roadway"/>
        //    <xs:field xpath="@name"/>
        //  </xs:unique>
        //  <xs:unique name="uGradeModelName">
        //    <xs:selector xpath="GradeModel"/>
        //    <xs:field xpath="@name"/>
        //  </xs:unique>
        //  <xs:key name="MonumentKey">
        //    <xs:selector xpath=".//Monuments/Monument"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:keyref name="SurveyMonument" refer="MonumentKey">
        //    <xs:selector xpath=".//SurveyMonument"/>
        //    <xs:field xpath="@mntRef"/>
        //  </xs:keyref>
        //  <xs:key name="StructKey">
        //    <xs:selector xpath=".//PipeNetwork/*/Struct"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:keyref name="PipeStart" refer="StructKey">
        //    <xs:selector xpath=".//PipeNetwork/*/Pipe"/>
        //    <xs:field xpath="@refStart"/>
        //  </xs:keyref>
        //  <xs:keyref name="PipeEnd" refer="StructKey">
        //    <xs:selector xpath=".//PipeNetwork/*/Pipe"/>
        //    <xs:field xpath="@refEnd"/>
        //  </xs:keyref>
        //  <xs:key name="PipeKey">
        //    <xs:selector xpath=".//PipeNetwork/*/Pipe"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:keyref name="StructInvert" refer="PipeKey">
        //    <xs:selector xpath=".//PipeNetwork/*/Struct/Invert"/>
        //    <xs:field xpath="@refPipe"/>
        //  </xs:keyref>
        //  <xs:key name="ReducedObservationName">
        //    <xs:selector xpath=".//*/ObservationGroup/*/ReducedObservation"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:key name="ReducedArcObservationName">
        //    <xs:selector xpath=".//*/ObservationGroup/*/ReducedArcObservation"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:key name="RedHorizontalPositionName">
        //    <xs:selector xpath=".//*/ObservationGroup/*/RedHorizontalPosition"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:key name="RedVerticalPositionName">
        //    <xs:selector xpath=".//*/ObservationGroup/*/RedVerticalPosition"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:key name="Coord3DGeomName">
        //    <xs:selector xpath=".//Parcels/*/Coord3DGeom"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:key name="AnnotationName">
        //    <xs:selector xpath=".//Survey/*/Annotation"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //  <xs:key name="SurveyorCertificateName">
        //    <xs:selector xpath=".//Survey/*/SurveyorCertificate"/>
        //    <xs:field xpath="@name"/>
        //  </xs:key>
        //</xs:element>
        #endregion

        #region Required
        /// <summary>
        /// All angular and direction values default to radians unless otherwise noted. Angular values, expressed in the specified Units.
        /// angleUnit are measured counter-clockwise from east=0. Horizontal directions, expressed in the specified Units.directionUnit are measured counter-clockwise from 0 degrees = north
        /// </summary>
        IList<IUnitElement> Units { get; set; }
        DateTime Date { get; set; }
        TimeSpan Time { get; set; }
        string Version { get; set; }
        #endregion

        #region Optional
        string Language { get; set; }
        bool ReadOnly { get; set; }
        int LandXmlId { get; set; }
        int Crc { get; set; }
        ICoordinateSystemElement CoordinateSystem { get; set; }
        IProjectElement Project { get; set; }
        IApplicationElement Application { get; set; }
        IAlignmentsElement Alignments { get; set; }
        IList<IFeatureDictionary> FeatureDictionary { get; set; } 
        #endregion
    }
}
