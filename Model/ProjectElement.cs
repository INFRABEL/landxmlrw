﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model
{
    public class ProjectElement : IProjectElement
    {
        public string Name { get; set; }

        public StateType? State { get; set; }

        public string Description { get; set; }

        private IList<IFeatureElement> _features = new List<IFeatureElement>();
        public IList<IFeatureElement> Features
        {
            get { return _features; }
            set
            {
                _features = value;
            }
        }
    }
}
