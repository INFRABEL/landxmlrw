﻿using System.Collections.Generic;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model
{
    /// <summary>
    /// Used to describe specific Feature code / property type values. DocFileRef points to reference documentation
    /// Each Property element defines one piece of data.
    /// </summary>
    public interface IFeatureDictionary
    {
        #region XSD Documentation 1.2
        //<xs:element name="FeatureDictionary">
        //  <xs:annotation>
        //    <xs:documentation>Used to describe specific Feature code / property type values. DocFileRef points to reference documentation</xs:documentation>
        //    <xs:documentation>Each Property element defines one piece of data.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:element ref="DocFileRef" minOccurs="0" maxOccurs="unbounded"/>
        //    </xs:sequence>
        //    <xs:attribute name="name" type="xs:string" use="required"/>
        //    <xs:attribute name="version" type="xs:string" use="optional"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion
        #region Required
        string Name { get; set; }
        #endregion
        #region Optional
        IList<IDocFileRefElement> DocFileRefs { get; set; } 
        string Version { get; set; }
        #endregion
    }
}
