﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LANDXMLRW.Model.Feature
{
    public class PropertyElement : IPropertyElement
    {
        public string Label { get; set; }

        public string Value { get; set; }
    }
}
