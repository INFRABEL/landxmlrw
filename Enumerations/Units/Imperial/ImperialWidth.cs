﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialWidth
    {
        // ReSharper disable InconsistentNaming
        foot,
        USSurveyFoot,
        inch
        // ReSharper restore InconsistentNaming
    }
}
