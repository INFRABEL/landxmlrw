﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.Profile
{
    public interface IProfSurf
    {
        #region XSD Documentation 1.2
        //<xs:element name="ProfSurf">
        //  <xs:annotation>
        //    <xs:documentation>The "ProfSurf" element will typically represent an existing ground surface for a profile. </xs:documentation>
        //    <xs:documentation>It is defined with a space delimited PntList2D of station/elevations pairs. </xs:documentation>
        //    <xs:documentation>Example: "0.000 86.52 6.267 86.89 12.413 87.01 26.020 87.83" </xs:documentation>
        //    <xs:documentation>Note: Gaps in the profile are handled by having 2 or more PntList2D elements.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:element ref="PntList2D" maxOccurs="unbounded"/>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //    </xs:sequence>
        //    <xs:attribute name="name" type="xs:string" use="required"/>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="state" type="stateType"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Sequence
        IList<IPointElement> Point2DList { get; set; }
        IList<IFeatureElement> Features { get; set; } 
        #endregion

        #region Required
        string Name { get; set; }
        #endregion

        #region Optional
        string Description { get; set; }
        StateType? State { get; set; }
        #endregion
    }
}
