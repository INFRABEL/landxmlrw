﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialPressure
    {
        // ReSharper disable InconsistentNaming
        inchHG,
        inHG
        // ReSharper restore InconsistentNaming
    }
}
