﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model.Profile
{
    public interface IProfile
    {
        #region XSD Documentation 1.2
        //<xs:element name="Profile">
        //  <xs:annotation>
        //    <xs:documentation>A profile or long section</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:choice maxOccurs="unbounded">
        //        <xs:element ref="ProfSurf" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="ProfAlign" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:choice>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //    </xs:sequence>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="name" type="xs:string"/>
        //    <xs:attribute name="staStart" type="xs:double"/>
        //    <xs:attribute name="state" type="stateType"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Sequence
        IList<IProfSurf> ProfileSurfs { get; set; }
        IList<IProfAlign> ProfAligns { get; set; }
        IList<IFeatureElement> Features { get; set; }
        #endregion

        #region Optional
        string Description { get; set; }
        string Name { get; set; }
        double StationStart { get; set; }
        StateType State { get; set; }
        #endregion
    }
}
