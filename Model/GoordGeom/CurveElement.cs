﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.GoordGeom
{
  [Serializable]
  [XmlType(TypeName = "Curve")]
  public class CurveElement : ICurveElement
  {
    public ClockWise Rotation { get; set; }

    [XmlAttribute(AttributeName = "chord")]
    public double Chord { get; set; }

    public CurveType CurveType { get; set; }

    public double Delta { get; set; }

    public string Description { get; set; }

    public double DirectionEnd { get; set; }

    public double DirectionStart { get; set; }

    public double External { get; set; }

    public double Length { get; set; }

    public double MidOrd { get; set; }

    public string Name { get; set; }

    public double Radius { get; set; }

    public double StationStart { get; set; }
    
    public double StationEnd{ get { return StationStart + Length; } }

    public StateType? State { get; set; }

    public double Tangent { get; set; }

    public string OId { get; set; }

    public string Note { get; set; }

    public IPointTypeElement Center { get; set; }

    public IPointTypeElement Start { get; set; }

    public IPointTypeElement End { get; set; }

    public IPointTypeElement Pi { get; set; }

    public IList<Feature.IFeatureElement> Features { get; set; }
  }
}
