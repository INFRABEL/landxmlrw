﻿namespace LANDXMLRW.Enumerations
{
    public enum CurveType
    {
        // ReSharper disable InconsistentNaming
        arc,
        chord
        // ReSharper restore InconsistentNaming
    }
}
