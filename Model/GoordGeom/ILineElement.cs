﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.GoordGeom
{
    /// <summary>
    /// 
    /// </summary>
    public interface ILineElement : IGeoElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Line">
        //<xs:annotation>
        //<xs:documentation>
        //Modified to include official ID, as with all CoordGeom elements
        //</xs:documentation>
        //</xs:annotation>
        //<xs:complexType>
        //<xs:sequence>
        //<xs:element ref="Start"/>
        //<xs:element ref="End"/>
        //<xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //</xs:sequence>
        //<xs:attribute name="desc" type="xs:string"/>
        //<xs:attribute name="dir" type="direction"/>
        //<xs:attribute name="length" type="xs:double"/>
        //<xs:attribute name="name" type="xs:string"/>
        //<xs:attribute name="staStart" type="xs:double"/>
        //<xs:attribute name="state" type="stateType"/>
        //<xs:attribute name="oID" type="xs:string"/>
        //<xs:attribute name="note" type="xs:string"/>
        //</xs:complexType>
        //</xs:element>
        #endregion

        #region Required (Sequence!!)
        /// <summary>
        /// Represents a 2D or 3D Starting Point
        /// </summary>
        IPointTypeElement Start { get; set; }
        /// <summary>
        /// Represents a 2D or 3D Ending Point
        /// </summary>
        IPointTypeElement End { get; set; }
        /// <summary>
        /// Used to include additional information that is not explicitly defined by the LandXML schema
        /// Feature may contain one or more Property, DocFileRef or nested Feature Elements. 
        /// Note: to allow any valid content, the explicit definitions for Property, DocFileRef and Feature habe been commented out, but are still excpected in common use.
        /// </summary>
        IList<IFeatureElement> Features { get; set; }
        #endregion

        #region Optional
        /// <summary>
        /// 
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Represents a normalized angular value that indicates a horizontal direction, expressed in the specified Direction units. Assume 0 degrees = north
        /// </summary>
        double Direction { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double Length { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double StationStart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double StationEnd { get; }
        /// <summary>
        /// 
        /// </summary>
        StateType? State { get; set; }
        /// <summary>
        /// Official ID
        /// </summary>
        string OId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Note { get; set; }
        #endregion
    }
}
