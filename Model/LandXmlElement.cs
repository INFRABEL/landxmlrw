﻿using System;
using System.Collections.Generic;
using LANDXMLRW.Model.Alignment;
using LANDXMLRW.Model.Units;

namespace LANDXMLRW.Model
{
    [Serializable]
    public class LandXmlElement : ILandXmlElement
    {
        private IList<IUnitElement> _units = new List<IUnitElement>();
        public IList<IUnitElement> Units
        {
            get
            {
                return _units;
            }
            set { _units = value; }
        }

        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }
            set { _date = value; }
        }

        private TimeSpan _time;
        public TimeSpan Time
        {
            get
            {
                return _time;
            }
            set { _time = value; }
        }

        private string _version;
        public string Version
        {
            get
            {
                return _version;
            }
            set { _version = value; }
        }

        private string _language;
        public string Language
        {
            get
            {
                return _language;
            }
            set { _language = value; }
        }

        private bool _readOnly;
        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set { _readOnly = value; }
        }

        private int _landXmlId;
        public int LandXmlId
        {
            get
            {
                return _landXmlId;
            }
            set { _landXmlId = value; }
        }

        private int _crc;
        public int Crc
        {
            get
            {
                return _crc;
            }
            set { _crc = value; }
        }

        private ICoordinateSystemElement _coordinateSystem;
        public ICoordinateSystemElement CoordinateSystem
        {
            get
            {
                return _coordinateSystem;
            }
            set { _coordinateSystem = value; }
        }

        private IProjectElement _project = new ProjectElement();
        public IProjectElement Project
        {
            get
            {
                return _project;
            }
            set { _project = value; }
        }

        private IApplicationElement _application = new ApplicationElement();
        public IApplicationElement Application
        {
            get
            {
                return _application;
            }
            set { _application = value; }
        }

        private IAlignmentsElement _alignments = new AlignmentsElement();
        public IAlignmentsElement Alignments
        {
            get
            {
                return _alignments;
            }
            set { _alignments = value; }
        }

        private IList<IFeatureDictionary> _featureDictionary = new List<IFeatureDictionary>();
        public IList<IFeatureDictionary> FeatureDictionary
        {
            get
            {
                return _featureDictionary;
            }
            set { _featureDictionary = value; }
        }
    }
}
