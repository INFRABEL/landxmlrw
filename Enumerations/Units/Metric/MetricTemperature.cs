﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricTemperature
    {
        // ReSharper disable InconsistentNaming
        celsius,
        kelvin
        // ReSharper restore InconsistentNaming
    }
}