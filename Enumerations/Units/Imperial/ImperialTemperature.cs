﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialTemperature
    {
        // ReSharper disable InconsistentNaming
        fahrenheit,
        kelvin
        // ReSharper restore InconsistentNaming
    }
}
