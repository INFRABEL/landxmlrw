﻿namespace LANDXMLRW.Model.Geometry
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPoint3DOptElement : IPointElement
    {
        #region XSD Documentation 1.2
        //<xs:simpleType name="Point3dOpt">
        //  <xs:restriction base="Point">
        //    <xs:minLength value="0"/>
        //    <xs:maxLength value="3"/>
        //  </xs:restriction>
        //</xs:simpleType>
        #endregion

        #region Required
        #endregion

        #region Optional
        #endregion
    }
}
