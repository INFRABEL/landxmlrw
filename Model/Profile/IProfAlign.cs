﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.Profile
{
    public interface IProfAlign
    {
        #region XSD Documentation 1.2
        //<xs:element name="ProfAlign">
        //  <xs:annotation>
        //    <xs:documentation>The "ProfAlign" element will typically represent a proposed vertical alignment for a profile.</xs:documentation>
        //    <xs:documentation>It is defined by a sequential series of any combination of the four "PVI" element types.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:choice maxOccurs="unbounded">
        //        <xs:element ref="PVI" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="ParaCurve" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="UnsymParaCurve" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="CircCurve" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:choice>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //      <!--               <xs:element ref="PVI"/>  -->
        //      <!--                 <xs:element ref="PVI"/> -->
        //    </xs:sequence>
        //    <xs:attribute name="name" type="xs:string" use="required"/>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="state" type="stateType"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Choice
        IList<IPointTypeElement> Pvi { get; set; }
        IList<IParaCurve> ParaCurves { get; set; }
        IList<IUnsymParaCurve> UnsymParaCurves { get; set; }
        IList<ICircCurve> CircCurves { get; set; }
        IList<IFeatureElement> Features { get; set; }
        #endregion

        #region Required
        string Name { get; set; }
        #endregion

        #region Optional
        string Description { get; set; }
        StateType? State { get; set; }
        #endregion
    }
}
