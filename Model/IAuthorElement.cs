﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LANDXMLRW.Model
{
    /// <summary>
    /// Optional element to identify the source of the file.
    /// </summary>
    public interface IAuthorElement
    {
        #region XSD Documentation 1.2

        // <xs:element name="Author">
        //  <xs:annotation>
        //    <xs:documentation>Optional element to identify the source of the file.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType mixed="true">
        //    <xs:sequence>
        //      <xs:any namespace="##other" processContents="skip" minOccurs="0"/>
        //    </xs:sequence>
        //    <xs:attribute name="createdBy" type="xs:string"/>
        //    <xs:attribute name="createdByEmail" type="xs:string"/>
        //    <xs:attribute name="company" type="xs:string"/>
        //    <xs:attribute name="companyURL" type="xs:string"/>
        //    <xs:attribute name="timeStamp" type="xs:dateTime" use="optional"/>
        //    <xs:anyAttribute/>
        //  </xs:complexType>
        //</xs:element>

        #endregion

        #region Optional

        string CreatedBy { get; set; }
        string Email { get; set; }
        string Company { get; set; }
        string CompanyUrl { get; set; }
        DateTime TimeStamp { get; set; }
    }

    #endregion
}
