﻿using System.Collections.Generic;
using System.Xml;
using LANDXMLRW.Model;

namespace LANDXMLRW.Services
{
    public interface ILandXmlService
    {
        /// <summary>
        /// This function will validate a LandXml file vs its xsd schema.
        /// </summary>
        /// <param name="xmlPath">Path to the xml file on disk.</param>
        /// <returns>XmlValidator object class, containing property's where validation exceptions can be read.</returns>
        XmlValidator Validate(string xmlPath);

        bool Validate(ILandXmlElement landXml);
        ILandXmlElement Read(XmlDocument document);
        void Write(ILandXmlElement landXml, string location);
    }
}
