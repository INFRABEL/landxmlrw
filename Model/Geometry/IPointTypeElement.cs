﻿namespace LANDXMLRW.Model.Geometry
{
    /// <summary>
    /// All elements derived from PointType will either contain a coordinate text value ( "north east" or "north east elev"), a "pntRef" attribute value, or both. The "pntRef" attribute contains the value of a PointType derived element "name" attribute that exists elsewhere the instance data. 
    /// If this element has a "pntRef" value, then it's coordinates will be retrieved from the referenced element. If an element contains both a coordinate value and a pntRef, the coordinate value should be used as the point location and the referenced point is either ignored or is used for point attributes such as number or desc.
    /// documentation
    /// The featureRef attribute points to a specific named Feature element that contains feature data related to the point.
    /// The suggested form is to refer to a feature element within the same CgPoints group or parent element of the point element.
    /// </summary>
    public interface IPointTypeElement : IPoint3DOptElement
    {
        #region XSD Documentation 1.2

        //<xs:complexType name="PointType" mixed="true">
        //  <xs:annotation>
        //    <xs:documentation>All elements derived from PointType will either contain a coordinate text value ( "north east" or "north east elev"), a "pntRef" attribute value, or both. The "pntRef" attribute contains the value of a PointType derived element "name" attribute that exists elsewhere the instance data. If this element has a "pntRef" value, then it's coordinates will be retrieved from the referenced element. If an element contains both a coordinate value and a pntRef, the coordinate value should be used as the point location and the referenced point is either ignored or is used for point attributes such as number or desc.</xs:documentation>
        //    <xs:documentation>The featureRef attribute points to a specific named Feature element that contains feature data related to the point.
        //The suggested form is to refer to a feature element within the same CgPoints group or parent element of the point element.
        //</xs:documentation>
        //  </xs:annotation>
        //  <xs:simpleContent>
        //    <xs:extension base="Point3dOpt">
        //      <xs:attribute name="name" type="xs:string"/>
        //      <xs:attribute name="desc" type="xs:string"/>
        //      <xs:attribute name="code" type="xs:string"/>
        //      <xs:attribute name="state" type="stateType"/>
        //      <xs:attribute name="pntRef" type="pointNameRef"/>
        //      <xs:attribute name="featureRef" type="featureNameRef" use="optional"/>
        //      <xs:attribute name="pointGeometry" type="pointGeometryType"/>
        //      <xs:attribute name="DTMAttribute" type="DTMAttributeType"/>
        //      <xs:attribute name="timeStamp" type="xs:dateTime" use="optional"/>
        //      <xs:attribute name="role" type="surveyRoleType" use="optional"/>
        //      <xs:attribute name="determinedTimeStamp" type="xs:dateTime" use="optional"/>
        //      <xs:attribute name="ellipsoidHeight" type="ellipsoidHeightType" use="optional"/>
        //      <xs:attribute name="latitude" type="latLongAngle" use="optional"/>
        //      <xs:attribute name="longitude" type="latLongAngle" use="optional"/>
        //      <xs:attribute name="zone" type="xs:string" use="optional"/>
        //      <xs:attribute name="northingStdError" type="xs:double" use="optional"/>
        //      <xs:attribute name="eastingStdError" type="xs:double" use="optional"/>
        //      <xs:attribute name="elevationStdError" type="xs:double" use="optional"/>
        //    </xs:extension>
        //  </xs:simpleContent>
        //</xs:complexType>

        #endregion

        #region Required
        #endregion

        #region Optional
        #endregion
    }
}
