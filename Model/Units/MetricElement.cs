﻿using LANDXMLRW.Enumerations.Units;
using LANDXMLRW.Enumerations.Units.Metric;

namespace LANDXMLRW.Model.Units
{
    public class MetricElement : IMetricElement
    {
        public MetricArea AreaUnit { get; set; }

        public MetricLinear LinearUnit { get; set; }

        public MetricVolume VolumeUnit { get; set; }

        public MetricTemperature TemperatureUnit { get; set; }

        public MetricPressure PressureUnit { get; set; }

        public MetricDiameter? DiameterUnit { get; set; }

        public MetricWidth? WidthUnit { get; set; }

        public MetricHeight? HeightUnit { get; set; }

        public MetricVelocity? VelocityUnit { get; set; }

        public MetricFlow? FlowUnit { get; set; }

        public string AngularUnit { get; set; }

        public string DirectionUnit { get; set; }

        public string LatLongAngularUnit { get; set; }

        public ElevationType? ElevationUnit { get; set; }
    }
}