﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model.Alignment
{
    /// <summary>
    /// A collection of horizontal Alignments
    /// </summary>
    public interface IAlignmentsElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Alignments">
        //  <xs:annotation>
        //    <xs:documentation>A collection of horizontal Alignments</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:element ref="Alignment" maxOccurs="unbounded"/>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //    </xs:sequence>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="name" type="xs:string"/>
        //    <xs:attribute name="state" type="stateType"/>
        //  </xs:complexType>
        //  <xs:unique name="uAlnName">
        //    <xs:selector xpath="Alignment"/>
        //    <xs:field xpath="@name"/>
        //  </xs:unique>
        //</xs:element>
        #endregion

        #region Required
        IList<IAlignmentElement> Alignments { get; set; }
        IList<IFeatureElement> Features { get; set; }
        #endregion

        #region Optional
        string Description { get; set; }
        string Name { get; set; }
        StateType State { get; set; }
        #endregion
    }
}
