﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricHeight
    {
        // ReSharper disable InconsistentNaming
        millimeter,
        centimeter,
        meter,
        kilometer
        // ReSharper restore InconsistentNaming
    }
}
