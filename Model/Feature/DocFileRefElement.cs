﻿namespace LANDXMLRW.Model.Feature
{
    public class DocFileRefElement : IDocFileRefElement
    {
        public string Name { get; set; }

        public string Location { get; set; }

        public string FileType { get; set; }

        public string FileFormat { get; set; }
    }
}
