﻿namespace LANDXMLRW.Model.Feature
{
    /// <summary>
    /// A reference to any external document file containing related information for the associated element.
    /// </summary>
    public interface IDocFileRefElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="DocFileRef">
        //  <xs:annotation>
        //    <xs:documentation>A reference to any external document file containing related information for the associated element.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:attribute name="name" use="required"/>
        //    <xs:attribute name="location" type="xs:anyURI" use="required"/>
        //    <xs:attribute name="fileType" type="xs:string"/>
        //    <xs:attribute name="fileFormat" type="xs:string"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        //ToDo: Name doesn't have a type??
        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Uniform Resource Identifier (URI) reference
        /// </summary>
        string Location { get; set; }
        #endregion

        #region Optional
        string FileType { get; set; }
        string FileFormat { get; set; }
        #endregion
    }
}
