﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    /// <summary>
    /// 
    /// </summary>
    public enum MetricArea
    {
        // ReSharper disable InconsistentNaming
        hectare,
        squareMeter,
        squareMillimeter,
        squareCentimeter
        // ReSharper restore InconsistentNaming
    }
}
