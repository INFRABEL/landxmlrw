﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Enumerations.Units;
using LANDXMLRW.Enumerations.Units.Imperial;
using LANDXMLRW.Enumerations.Units.Metric;
using LANDXMLRW.Model;
using LANDXMLRW.Model.Alignment;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;
using LANDXMLRW.Model.GoordGeom;
using LANDXMLRW.Model.Profile;
using LANDXMLRW.Model.Units;

namespace LANDXMLRW.Services
{
    public class LandXmlService : ILandXmlService
    {


        public bool Validate(ILandXmlElement landXml)
        {
            throw new NotImplementedException();
        }

        public XmlValidator Validate(string xmlPath)
        {
            // We create a new XmlValidator, and leave out the xsd path, the calling function
            // should be able to resolve the xsd file if an internet connection is available. 
            var validator = new XmlValidator(xmlPath);
            validator.Validate();
            return null;
        }

        public void Write(ILandXmlElement landXml, string location)
        {
            throw new NotImplementedException();
        }

        private IList<IUnitElement> MapUnits(XmlNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            if (node.Name != "Units")
                throw new ArgumentOutOfRangeException("node");

            IList<IUnitElement> units = new List<IUnitElement>();

            var unit = node.ChildNodes[0];

            if (unit != null)
            {
                if (unit.Name == "Metric")
                {
                    var metric = new MetricElement();

                    foreach (XmlAttribute xmlAttribute in unit)
                    {
                        if (xmlAttribute.Name == "areaUnit")
                            metric.AreaUnit = ConvertMetricArea(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "linearUnit")
                            metric.LinearUnit = ConvertMetricLinear(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "volumeUnit")
                            metric.VolumeUnit = ConvertMetricVolume(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "temperatureUnit")
                            metric.TemperatureUnit = ConvertMetricTemperature(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "pressureUnit")
                            metric.PressureUnit = ConvertMetricPressure(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "diameterUnit")
                            metric.DiameterUnit = ConvertMetricDiameter(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "widthUnit")
                            metric.WidthUnit = ConvertMetricWidth(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "heightUnit")
                            metric.HeightUnit = ConvertMetricHeight(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "velocityUnit")
                            metric.VelocityUnit = ConvertMetricVelocity(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "flowUnit")
                            metric.FlowUnit = ConvertMetricFlow(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "angularUnit")
                            metric.AngularUnit = Convert.ToString(xmlAttribute.Value);
                        if (xmlAttribute.Name == "directionUnit")
                            metric.DirectionUnit = Convert.ToString(xmlAttribute.Value);
                        if (xmlAttribute.Name == "latLongAngularUnit")
                            metric.LatLongAngularUnit = Convert.ToString(xmlAttribute.Value);
                        if (xmlAttribute.Name == "elevationUnit")
                            metric.ElevationUnit = ConvertElevationType(Convert.ToString(xmlAttribute.Value));
                    }

                    units.Add(metric);
                }
                if (unit.Name == "Imperial")
                {
                    var imperial = new ImperialElement();

                    foreach (XmlAttribute xmlAttribute in unit)
                    {
                        if (xmlAttribute.Name == "areaUnit")
                            imperial.AreaUnit = ConvertImperialArea(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "linearUnit")
                            imperial.LinearUnit = ConvertImperialLinear(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "volumeUnit")
                            imperial.VolumeUnit = ConvertImperialVolume(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "temperatureUnit")
                            imperial.TemperatureUnit =
                                ConvertImperialTemperature(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "pressureUnit")
                            imperial.PressureUnit = ConvertImperialPressure(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "diameterUnit")
                            imperial.DiameterUnit = ConvertImperialDiameter(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "widthUnit")
                            imperial.WidthUnit = ConvertImperialWidth(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "heightUnit")
                            imperial.HeightUnit = ConvertImperialHeigth(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "velocityUnit")
                            imperial.VelocityUnit = ConvertImperialVelocity(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "flowUnit")
                            imperial.FlowUnit = ConvertImperialFlow(Convert.ToString(xmlAttribute.Value));
                        if (xmlAttribute.Name == "angularUnit")
                            imperial.AngularUnit = Convert.ToString(xmlAttribute.Value);
                        if (xmlAttribute.Name == "directionUnit")
                            imperial.DirectionUnit = Convert.ToString(xmlAttribute.Value);
                        if (xmlAttribute.Name == "latLongAngularUnit")
                            imperial.LatLongAngularUnit = Convert.ToString(xmlAttribute.Value);
                        if (xmlAttribute.Name == "elevationUnit")
                            imperial.ElevationUnit = ConvertElevationType(Convert.ToString(xmlAttribute.Value));
                    }

                    units.Add(new ImperialElement());
                }
            }
            return units;
        }

        private IProjectElement MapProject(XmlNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            if (node.Name != "Project")
                throw new ArgumentOutOfRangeException("node");

            var project = new ProjectElement();

            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Feature")
                {
                    project.Features = MapFeature(child);
                }
            }
            if (node.Attributes != null)
            {
                foreach (XmlAttribute lXmlAttribute in node.Attributes)
                {
                    if (lXmlAttribute.Name == "name")
                        project.Name = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "desc")
                        project.Description = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "state")
                        project.State = ConvertState(Convert.ToString(lXmlAttribute.Value));
                }
            }
            return project;
        }

        private IApplicationElement MapApplication(XmlNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            if (node.Name != "Application")
                throw new ArgumentOutOfRangeException("node");

            var application = new ApplicationElement();

            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Author")
                    application.Author = MapAuthor(child);
            }
            if (node.Attributes != null)
            {
                foreach (XmlAttribute lXmlAttribute in node.Attributes)
                {
                    if (lXmlAttribute.Name == "name")
                        application.Name = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "desc")
                        application.Description = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "manufacturer")
                        application.Manufacturer = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "version")
                        application.Version = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "manufacturerUrl")
                        application.ManufacturerUrl = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "timeStamp")
                        application.TimeStamp = Convert.ToDateTime(lXmlAttribute.Value);
                }
            }
            return application;
        }

        private IAuthorElement MapAuthor(XmlNode node)
        {
            if (node == null)
                throw new ArgumentNullException("node");

            if (node.Name != "Author")
                throw new ArgumentOutOfRangeException("node");

            var author = new AuthorElement();

            if (node.Attributes != null)
            {
                foreach (XmlAttribute lXmlAttribute in node.Attributes)
                {
                    if (lXmlAttribute.Name == "createdBy")
                        author.CreatedBy = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "createdByEmail")
                        author.Email = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "company")
                        author.Company = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "companyURL")
                        author.CompanyUrl = Convert.ToString(lXmlAttribute.Value);
                    if (lXmlAttribute.Name == "timeStamp")
                        author.TimeStamp = Convert.ToDateTime(lXmlAttribute.Value);
                }
            }
            return author;
        }

        private AlignmentsElement MapAlignments(XmlNode node)
        {
            var alignments = new AlignmentsElement();

            if (node.Attributes != null)
            {
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    if (attribute.Name == "name")
                        alignments.Name = attribute.Value;
                    if (attribute.Name == "desc")
                        alignments.Description = attribute.Value;
                    if (attribute.Name == "state")
                        alignments.State = ConvertState(attribute.Value);
                }
            }
            //Get all Alignment Element
            foreach (XmlNode child in node.ChildNodes)
            {
                var al = new AlignmentElement();

                if (child.Attributes != null)
                {
                    foreach (XmlAttribute attribute in child.Attributes)
                    {
                        if (attribute.Name == "name")
                            al.Name = attribute.Value;
                        if (attribute.Name == "length")
                            al.Length = ConvertDouble(attribute.Value);
                        if (attribute.Name == "staStart")
                            al.StaStart = ConvertDouble(attribute.Value);
                        if (attribute.Name == "desc")
                            al.Description = attribute.Value;
                        if (attribute.Name == "oID")
                            al.OId = attribute.Value;
                        if (attribute.Name == "state")
                            al.State = ConvertState(attribute.Value);
                    }
                }

                foreach (XmlNode c2 in child.ChildNodes)
                {
                    if (c2.Name == "Start")
                    {
                        //Todo : complete read
                        var start = new Point3DOptElement();

                    }
                    if (c2.Name == "CoordGeom")
                    {
                        foreach (XmlNode c3 in c2.ChildNodes)
                        {
                            al.CoordGeom.Add(MapCoordGeom(c3));
                        }
                    }
                    if (c2.Name == "Feature")
                    {
                        al.Features = MapFeature(c2);
                    }
                    if (c2.Name == "Profile")
                    {
                        al.Profile = MapProfile(c2);
                    }
                    if (c2.Name == "Cant")
                    {
                        al.Cant = MapCant(c2);
                    }
                }

                alignments.Alignments.Add(al);
            }

            return alignments;
        }

        private IGeoElement MapCoordGeom(XmlNode xmlNode)
        {
            #region Line

            if (xmlNode.Name == "Line")
            {
                ILineElement geomElement = new LineElement();

                foreach (XmlNode node in xmlNode.ChildNodes)
                {
                    if (node.Name == "Feature")
                    {
                        geomElement.Features = MapFeature(node);
                    }
                }

                if (xmlNode.Attributes != null)
                {
                    foreach (XmlAttribute attribute in xmlNode.Attributes)
                    {
                        if (attribute.Name == "desc")
                            geomElement.Description = attribute.Value;
                        if (attribute.Name == "dir")
                            geomElement.Direction = ConvertDouble(attribute.Value);
                        if (attribute.Name == "length")
                            geomElement.Length = ConvertDouble(attribute.Value);
                        if (attribute.Name == "name")
                            geomElement.Name = attribute.Value;
                        if (attribute.Name == "staStart")
                            geomElement.StationStart = ConvertDouble(attribute.Value);
                        if (attribute.Name == "state")
                            geomElement.State = ConvertState(attribute.Value);
                        if (attribute.Name == "oID")
                            geomElement.OId = attribute.Value;
                        if (attribute.Name == "note")
                            geomElement.Note = attribute.Value;
                    }
                }
                foreach (XmlNode c4 in xmlNode.ChildNodes)
                {
                    //Todo: this might fail if the format is not correctly formatted!
                    if (c4.Name == "Start")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.Start = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                    //Todo: this might fail if the format is not correctly formatted!
                    if (c4.Name == "End")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.End = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                }

                return geomElement;
            }

            #endregion

            #region Curve

            if (xmlNode.Name == "Curve")
            {
                ICurveElement geomElement = new CurveElement();

                foreach (XmlNode node in xmlNode.ChildNodes)
                {
                    if (node.Name == "Feature")
                    {
                        geomElement.Features = MapFeature(node);
                    }
                }

                if (xmlNode.Attributes != null)
                {
                    foreach (XmlAttribute attribute in xmlNode.Attributes)
                    {
                        if (attribute.Name == "rot")
                            geomElement.Rotation = ConvertClockwise(attribute.Value);
                        if (attribute.Name == "chord")
                            geomElement.Chord = ConvertDouble(attribute.Value);
                        if (attribute.Name == "crvType")
                            geomElement.CurveType = ConvertCurveType(attribute.Value);
                        if (attribute.Name == "delta")
                            geomElement.Delta = ConvertDouble(attribute.Value);
                        if (attribute.Name == "desc")
                            geomElement.Description = attribute.Value;
                        if (attribute.Name == "dirEnd")
                            geomElement.DirectionEnd = ConvertDouble(attribute.Value);
                        if (attribute.Name == "dirStart")
                            geomElement.DirectionStart = ConvertDouble(attribute.Value);
                        if (attribute.Name == "external")
                            geomElement.External = ConvertDouble(attribute.Value);
                        if (attribute.Name == "length")
                            geomElement.Length = ConvertDouble(attribute.Value);
                        if (attribute.Name == "midOrd")
                            geomElement.MidOrd = ConvertDouble(attribute.Value);
                        if (attribute.Name == "name")
                            geomElement.Name = attribute.Value;
                        if (attribute.Name == "radius")
                            geomElement.Radius = ConvertDouble(attribute.Value);
                        if (attribute.Name == "staStart")
                            geomElement.StationStart = ConvertDouble(attribute.Value);
                        if (attribute.Name == "state")
                            geomElement.State = ConvertState(attribute.Value);
                        if (attribute.Name == "tangent")
                            geomElement.Tangent = ConvertDouble(attribute.Value);
                        if (attribute.Name == "oID")
                            geomElement.OId = attribute.Value;
                        if (attribute.Name == "note")
                            geomElement.Note = attribute.Value;
                    }
                }
                foreach (XmlNode c4 in xmlNode.ChildNodes)
                {
                    //Todo : this could be incorrectly formatted!
                    if (c4.Name == "Start")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.Start = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                    //Todo : this could be incorrectly formatted!
                    if (c4.Name == "End")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.End = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                    //Todo : this could be incorrectly formatted!
                    if (c4.Name == "Center")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.Center = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                }

                return geomElement;
            }

            #endregion

            #region Spiral

            if (xmlNode.Name == "Spiral")
            {
                ISpiralElement geomElement = new SpiralElement();

                foreach (XmlNode node in xmlNode.ChildNodes)
                {
                    if (node.Name == "Feature")
                    {
                        geomElement.Features = MapFeature(node);
                    }
                }

                if (xmlNode.Attributes != null)
                {
                    foreach (XmlAttribute attribute in xmlNode.Attributes)
                    {
                        if (attribute.Name == "length")
                            geomElement.Length = ConvertDouble(attribute.Value);
                        if (attribute.Name == "radiusEnd")
                            geomElement.RadiusEnd = ConvertDouble(attribute.Value);
                        //Todo: Can return INF or -INF
                        if (attribute.Name == "radiusStart")
                            geomElement.RadiusStart = ConvertDouble(attribute.Value);
                        if (attribute.Name == "rot")
                            geomElement.Rotation = ConvertClockwise(attribute.Value);
                        if (attribute.Name == "spiType")
                            geomElement.SpiralType = ConvertSpiralType(attribute.Value);
                        if (attribute.Name == "chord")
                            geomElement.Chord = ConvertDouble(attribute.Value);
                        if (attribute.Name == "constant")
                            geomElement.Constant = ConvertDouble(attribute.Value);
                        if (attribute.Name == "desc")
                            geomElement.Description = attribute.Value;
                        if (attribute.Name == "dirEnd")
                            geomElement.DirectionEnd = ConvertDouble(attribute.Value);
                        if (attribute.Name == "dirStart")
                            geomElement.DirectionStart = ConvertDouble(attribute.Value);
                        if (attribute.Name == "name")
                            geomElement.Name = attribute.Value;
                        if (attribute.Name == "theta")
                            geomElement.Theta = ConvertDouble(attribute.Value);
                        if (attribute.Name == "totalY")
                            geomElement.TotalY = ConvertDouble(attribute.Value);
                        if (attribute.Name == "totalX")
                            geomElement.TotalX = ConvertDouble(attribute.Value);
                        if (attribute.Name == "staStart")
                            geomElement.StationStart = ConvertDouble(attribute.Value);
                        if (attribute.Name == "state")
                            geomElement.State = ConvertState(attribute.Value);
                        if (attribute.Name == "tanLong")
                            geomElement.TanLong = ConvertDouble(attribute.Value);
                        if (attribute.Name == "tanShort")
                            geomElement.TanShort = ConvertDouble(attribute.Value);
                        if (attribute.Name == "oID")
                            geomElement.OId = attribute.Value;
                    }
                }
                foreach (XmlNode c4 in xmlNode.ChildNodes)
                {

                    //Todo : this could be incorrectly formatted!
                    if (c4.Name == "Start")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.Start = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                    //Todo : this could be incorrectly formatted!
                    if (c4.Name == "End")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.End = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                    //Todo : this could be incorrectly formatted!
                    if (c4.Name == "PI")
                    {
                        string[] split = c4.InnerText.Split(' ');
                        geomElement.Pi = new Point3DOptElement
                        {
                            X = ConvertDouble(split[0]),
                            Y = ConvertDouble(split[1])
                        };
                    }
                }

                return geomElement;
            }

            #endregion

            throw new ArgumentOutOfRangeException();
        }

        private double ConvertDouble(string value)
        {
            var provider = new NumberFormatInfo
            {
                NumberDecimalSeparator = ".",
                NumberGroupSeparator = "",
            };

            if (value == "INF")
                return Double.PositiveInfinity;
            if (value == "-INF")
                return Double.NegativeInfinity;

            if (Double.IsNaN(Convert.ToDouble(value)))
                throw new ArgumentOutOfRangeException("value");

            return Convert.ToDouble(value, provider);
        }

        private IList<IFeatureElement> MapFeature(XmlNode node)
        {
            if (node.Name != "Feature")
                throw new ArgumentOutOfRangeException("node");

            var list = new List<IFeatureElement>();

            //Todo: review code!!
            foreach (XmlElement el in node)
            {
                var feature = new FeatureElement();
                if (node.Attributes != null)
                {
                    foreach (XmlAttribute attribute in node.Attributes)
                    {
                        if (attribute.Name == "name")
                            feature.Name = attribute.Value;
                        if (attribute.Name == "code")
                            feature.Code = attribute.Value;
                        //Todo: Check if this is converted to the correct datatype
                        if (attribute.Name == "source")
                            feature.Source = attribute.Value;
                    }
                }

                foreach (XmlElement child in node.ChildNodes)
                {
                    if (child.Name == "Property")
                    {
                        var property = new PropertyElement();
                        foreach (XmlAttribute attribute in child.Attributes)
                        {
                            if (attribute.Name == "label")
                                property.Label = attribute.Value;
                            if (attribute.Name == "value")
                                property.Value = attribute.Value;
                        }
                        feature.Properties.Add(property);
                    }
                    if (child.Name == "DocFileRef")
                    {
                        var property = new DocFileRefElement();
                        foreach (XmlAttribute attribute in child.Attributes)
                        {
                            if (attribute.Name == "name")
                                property.Name = attribute.Value;
                            if (attribute.Name == "location")
                                property.Location = attribute.Value;
                            if (attribute.Name == "fileType")
                                property.FileType = attribute.Value;
                            if (attribute.Name == "fileFormat")
                                property.FileFormat = attribute.Value;
                        }
                        feature.DocFileReferences.Add(property);
                    }
                    if (child.Name == "Feature")
                    {
                        //Loop through the same code function
                        MapFeature(child);
                    }
                }
                list.Add(feature);
            }
            return list;
        }

        private IList<IPointElement> MapPoint2DList(XmlNode node)
        {
            if (node.Name != "PntList2D")
                throw new ArgumentOutOfRangeException("node");

            //Todo: add a check that will verify what should happen if the format or sequence of coordinates is incorrect.

            var l = new List<IPointElement>();

            var result = node.InnerText.Split(' ');

            for (var i = 0; i < result.Count(); i++)
            {
                var x = result[i];
                i++;
                var y = result[i];
                l.Add(new Point3DOptElement { X = ConvertDouble(x), Y = ConvertDouble(y) });
            }

            return l;
        }

        private IProfile MapProfile(XmlNode node)
        {
            var profile = new Profile();

            if (node.Attributes != null)
            {
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    if (attribute.Name == "desc")
                        profile.Description = attribute.Value;
                    if (attribute.Name == "name")
                        profile.Name = attribute.Value;
                    if (attribute.Name == "staStart")
                        profile.StationStart = ConvertDouble(attribute.Value);
                    if (attribute.Name == "state")
                        profile.State = ConvertState(attribute.Value);
                }
            }

            foreach (XmlElement element in node)
            {
                if (element.Name == "ProfSurf")
                {
                    //ToDo = check how to implement this correctly
                }
                if (element.Name == "ProfAlign")
                {
                    //ToDo = check how to implement this correctly
                }
                if (element.Name == "Feature")
                {
                    profile.Features = MapFeature(element);
                }
            }
            return profile;
        }

        private IProfSurf MapProfSurfs(XmlNode node)
        {
            //ToDo : complete + add features
            IProfSurf ps = new ProfSurf();

            foreach (XmlNode child in node.ChildNodes)
            {
                if (child.Name == "Feature")
                    ps.Features = MapFeature(child);
                if (child.Name == "PntList2D")
                    ps.Point2DList = MapPoint2DList(child);
            }

            foreach (XmlAttribute xmlAttribute in node)
            {
                if (xmlAttribute.Name == "name")
                    ps.Name = xmlAttribute.Value;
                if (xmlAttribute.Name == "desc")
                    ps.Description = xmlAttribute.Value;
                if (xmlAttribute.Name == "state")
                    ps.State = ConvertState(xmlAttribute.Value);
            }
            return ps;
        }

        private IList<IProfAlign> MapProfAligns(XmlNode node)
        {
            return null;
        }

        private ICoordinateSystemElement MapCoordinateSystem(XmlNode node)
        {
            ICoordinateSystemElement coordinateSystem = new CoordinateSystem();
            if (node.Attributes != null)
            {
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    if (attribute.Name == "desc")
                        coordinateSystem.Description = attribute.Value;
                    if (attribute.Name == "name")
                        coordinateSystem.Name = attribute.Value;
                    if (attribute.Name == "epsgCode")
                        coordinateSystem.EpsgCode = attribute.Value;
                    if (attribute.Name == "ogcWktCode")
                        coordinateSystem.OgcWktCode = attribute.Value;
                    if (attribute.Name == "horizontalDatum")
                        coordinateSystem.HorizontalDatum = attribute.Value;
                    if (attribute.Name == "ellipsoidName")
                        coordinateSystem.EllipsoidName = attribute.Value;
                    if (attribute.Name == "horizontalCoordinateSystemName")
                        coordinateSystem.HorizontalCoordinateSystemName = attribute.Value;
                    if (attribute.Name == "geocentricCoordinateSystemName")
                        coordinateSystem.GeocentricCoordinateSystemName = attribute.Value;
                    if (attribute.Name == "verticalDatum")
                        coordinateSystem.VerticalDatum = attribute.Value;
                    if (attribute.Name == "fileLocation")
                        coordinateSystem.FileLocation = attribute.Value;
                    if (attribute.Name == "rotationAngle")
                        coordinateSystem.RotationAngle = ConvertDouble(attribute.Value);
                    if (attribute.Name == "datum")
                        coordinateSystem.Datum = attribute.Value;
                    if (attribute.Name == "fittedCoordinateSystemName")
                        coordinateSystem.FittedCoordinateSystemName = attribute.Value;
                    if (attribute.Name == "compoundCoordinateSystemName")
                        coordinateSystem.CompoundCoordinateSystemName = attribute.Value;
                    if (attribute.Name == "localCoordinateSystemName")
                        coordinateSystem.LocalCoordinateSystemName = attribute.Value;
                    if (attribute.Name == "geographicCoordinateSystemName")
                        coordinateSystem.GeographicCoordinateSystemName = attribute.Value;
                    if (attribute.Name == "projectedCoordinateSystemName")
                        coordinateSystem.ProjectedCoordinateSystemName = attribute.Value;
                    if (attribute.Name == "verticalCoordinateSystemName")
                        coordinateSystem.VerticalCoordinateSystemName = attribute.Value;
                }
            }

            return coordinateSystem;
        }

        private ICant MapCant(XmlNode node)
        {
            ICant cant = new Cant();

            if (node.Attributes != null)
            {
                foreach (XmlAttribute attribute in node.Attributes)
                {
                    if (attribute.Name == "name")
                        cant.Name = attribute.Value;
                    if (attribute.Name == "gauge")
                        cant.Gauge = ConvertDouble(attribute.Value);
                    if (attribute.Name == "desc")
                        cant.Description = attribute.Value;
                    if (attribute.Name == "state")
                        cant.State = ConvertState(attribute.Value);
                    if (attribute.Name == "equilibriumConstant")
                        cant.EquilibriumConstant = ConvertDouble(attribute.Value);
                    if (attribute.Name == "appliedCantConstant")
                        cant.AppliedCantConstant = ConvertDouble(attribute.Value);
                    if (attribute.Name == "gauge")
                        cant.Gauge = ConvertDouble(attribute.Value);

                }
                foreach (XmlElement element in node)
                {
                    if (element.Name == "CantStation")
                    {
                        var cantStation = new CantStation();
                        foreach (XmlAttribute attribute in element.Attributes)
                        {
                            if (attribute.Name == "station")
                                cantStation.Station = ConvertDouble(attribute.Value);
                            if (attribute.Name == "equilibriumCant")
                                cantStation.EquilibriumCant = ConvertDouble(attribute.Value);
                            if (attribute.Name == "appliedCant")
                                cantStation.AppliedCant = ConvertDouble(attribute.Value);
                            if (attribute.Name == "cantDeficiency")
                                cantStation.CantDeficiency = ConvertDouble(attribute.Value);
                            if (attribute.Name == "cantExcess")
                                cantStation.CantExcess = ConvertDouble(attribute.Value);
                            if (attribute.Name == "rateOfChangeOfAppliedCantOverTime")
                                cantStation.RateOfChangeOfAppliedCantOverTime = ConvertDouble(attribute.Value);
                            if (attribute.Name == "rateOfChangeOfAppliedCantOverLength")
                                cantStation.RateOfChangeOfAppliedCantOverLength = ConvertDouble(attribute.Value);
                            if (attribute.Name == "rateOfChangeOfCantDeficiencyOverTime")
                                cantStation.RateOfChangeOfCantDeficiencyOverTime = ConvertDouble(attribute.Value);
                            if (attribute.Name == "cantGradient")
                                cantStation.CantGradient = ConvertDouble(attribute.Value);
                            if (attribute.Name == "speed")
                                cantStation.Speed = ConvertDouble(attribute.Value);
                            if (attribute.Name == "transitionType")
                                cantStation.TransitionType = ConvertSpiralType(attribute.Value);
                            if (attribute.Name == "curvature")
                                cantStation.Curvature = ConvertClockwise(attribute.Value);
                            if (attribute.Name == "adverse")
                                cantStation.Adverse = Convert.ToBoolean(attribute.Value);
                        }
                        cant.CantStations.Add(cantStation);
                    }
                    if (element.Name == "SpeedStation")
                    {
                        var speedStation = new SpeedStation();
                        foreach (XmlAttribute attribute in element.Attributes)
                        {
                            if (attribute.Name == "station")
                                speedStation.Station = ConvertDouble(attribute.Value);
                            if (attribute.Name == "speed")
                                speedStation.Speed = ConvertDouble(attribute.Value);
                        }
                        cant.SpeedStations.Add(speedStation);
                    }
                }
            }
            return cant;
        }

        public ILandXmlElement Read(XmlDocument document)
        {
            var landXml = new LandXmlElement();

            #region Attributes LandXml Rootnode Element

            XmlNode landXmlNode = document.ChildNodes[1];

            if (landXmlNode.Attributes != null)
            {
                foreach (XmlAttribute lXmlAttribute in landXmlNode.Attributes)
                {
                    if (lXmlAttribute.Name == "date")
                        landXml.Date = Convert.ToDateTime(lXmlAttribute.Value);

                    if (lXmlAttribute.Name == "time")
                    {
                        DateTime t = Convert.ToDateTime(lXmlAttribute.Value);
                        landXml.Time = t.TimeOfDay;
                    }

                    if (lXmlAttribute.Name == "version")
                        landXml.Version = Convert.ToString(lXmlAttribute.Value);

                    if (lXmlAttribute.Name == "language")
                        landXml.Language = Convert.ToString(lXmlAttribute.Value);

                    if (lXmlAttribute.Name == "readOnly")
                        landXml.ReadOnly = Convert.ToBoolean(lXmlAttribute.Value);

                    if (lXmlAttribute.Name == "LandXMLId")
                        landXml.LandXmlId = Convert.ToInt32(lXmlAttribute.Value);

                    if (lXmlAttribute.Name == "crc")
                        landXml.Crc = Convert.ToInt32(lXmlAttribute.Value);
                }
            }

            #endregion


            foreach (XmlNode node in landXmlNode.ChildNodes)
            {
                if (node.Name == "Units")
                {
                    landXml.Units = MapUnits(node);
                }
                if (node.Name == "CoordinateSystem")
                {
                    landXml.CoordinateSystem = MapCoordinateSystem(node);
                }
                if (node.Name == "Project")
                {
                    landXml.Project = MapProject(node);
                }
                if (node.Name == "Application")
                {
                    landXml.Application = MapApplication(node);
                }
                if (node.Name == "Alignments")
                {
                    landXml.Alignments = MapAlignments(node);
                }
                if (node.Name == "CgPoints")
                {

                }
                if (node.Name == "Amendment")
                {

                }
                if (node.Name == "GradeModel")
                {

                }
                if (node.Name == "Monuments")
                {

                }
                if (node.Name == "Parcels")
                {

                }
                if (node.Name == "PlanFeatures")
                {

                }
                if (node.Name == "PipeNetworks")
                {

                }
                if (node.Name == "Roadways")
                {

                }
                if (node.Name == "Surfaces")
                {

                }
                if (node.Name == "Survey")
                {

                }
                if (node.Name == "FeatureDictionary")
                {

                }
            }


            return landXml;
        }

        #region Mapping Enmerations
        private ClockWise ConvertClockwise(string clockWise)
        {
            if (clockWise == ClockWise.cw.ToString())
                return ClockWise.cw;
            if (clockWise == ClockWise.ccw.ToString())
                return ClockWise.ccw;
            throw new ArgumentOutOfRangeException("clockWise");
        }

        private SpiralType ConvertSpiralType(string spiralType)
        {
            if (spiralType == SpiralType.biquadratic.ToString())
                return SpiralType.biquadratic;
            if (spiralType == SpiralType.biquadraticParabola.ToString())
                return SpiralType.biquadraticParabola;
            if (spiralType == SpiralType.bloss.ToString())
                return SpiralType.bloss;
            if (spiralType == SpiralType.clothoid.ToString())
                return SpiralType.clothoid;
            if (spiralType == SpiralType.cosine.ToString())
                return SpiralType.cosine;
            if (spiralType == SpiralType.cubic.ToString())
                return SpiralType.cubic;
            if (spiralType == SpiralType.cubicParabola.ToString())
                return SpiralType.cubicParabola;
            if (spiralType == SpiralType.japaneseCubic.ToString())
                return SpiralType.japaneseCubic;
            if (spiralType == SpiralType.radioid.ToString())
                return SpiralType.radioid;
            if (spiralType == SpiralType.revBiquadratic.ToString())
                return SpiralType.revBiquadratic;
            if (spiralType == SpiralType.revBloss.ToString())
                return SpiralType.revBloss;
            if (spiralType == SpiralType.revCosine.ToString())
                return SpiralType.revCosine;
            if (spiralType == SpiralType.revSinusoid.ToString())
                return SpiralType.revSinusoid;
            if (spiralType == SpiralType.sineHalfWave.ToString())
                return SpiralType.sineHalfWave;
            if (spiralType == SpiralType.sinusoid.ToString())
                return SpiralType.sinusoid;
            if (spiralType == SpiralType.weinerBogen.ToString())
                return SpiralType.weinerBogen;
            throw new ArgumentOutOfRangeException("spiralType");
        }

        private StateType ConvertState(string state)
        {
            if (state == StateType.abandoned.ToString())
                return StateType.abandoned;
            if (state == StateType.destroyed.ToString())
                return StateType.destroyed;
            if (state == StateType.existing.ToString())
                return StateType.existing;
            if (state == StateType.proposed.ToString())
                return StateType.proposed;
            throw new ArgumentOutOfRangeException("state");
        }

        private MetricArea ConvertMetricArea(string area)
        {
            if (area == MetricArea.squareCentimeter.ToString())
                return MetricArea.squareCentimeter;
            if (area == MetricArea.squareMeter.ToString())
                return MetricArea.squareMeter;
            if (area == MetricArea.squareMillimeter.ToString())
                return MetricArea.squareMillimeter;
            throw new ArgumentOutOfRangeException("area");
        }

        private MetricLinear ConvertMetricLinear(string linear)
        {
            if (linear == MetricLinear.centimeter.ToString())
                return MetricLinear.centimeter;
            if (linear == MetricLinear.kilometer.ToString())
                return MetricLinear.kilometer;
            if (linear == MetricLinear.meter.ToString())
                return MetricLinear.meter;
            if (linear == MetricLinear.millimeter.ToString())
                return MetricLinear.millimeter;
            throw new ArgumentOutOfRangeException("linear");
        }

        private MetricVolume ConvertMetricVolume(string volume)
        {
            if (volume == MetricVolume.cubicMeter.ToString())
                return MetricVolume.cubicMeter;
            if (volume == MetricVolume.hectareMeter.ToString())
                return MetricVolume.hectareMeter;
            if (volume == MetricVolume.liter.ToString())
                return MetricVolume.liter;

            throw new ArgumentOutOfRangeException("volume");
        }

        private MetricTemperature ConvertMetricTemperature(string temperature)
        {
            if (temperature == MetricTemperature.celsius.ToString())
                return MetricTemperature.celsius;
            if (temperature == MetricTemperature.kelvin.ToString())
                return MetricTemperature.kelvin;

            throw new ArgumentOutOfRangeException("temperature");
        }

        private MetricPressure ConvertMetricPressure(string pressure)
        {
            if (pressure == MetricPressure.HPA.ToString())
                return MetricPressure.HPA;
            if (pressure == MetricPressure.milliBars.ToString())
                return MetricPressure.milliBars;
            if (pressure == MetricPressure.millimeterHG.ToString())
                return MetricPressure.millimeterHG;
            if (pressure == MetricPressure.mmHG.ToString())
                return MetricPressure.mmHG;

            throw new ArgumentOutOfRangeException("pressure");
        }

        private MetricDiameter ConvertMetricDiameter(string diameter)
        {
            if (diameter == MetricDiameter.centimeter.ToString())
                return MetricDiameter.centimeter;
            if (diameter == MetricDiameter.kilometer.ToString())
                return MetricDiameter.kilometer;
            if (diameter == MetricDiameter.meter.ToString())
                return MetricDiameter.meter;
            if (diameter == MetricDiameter.millimeter.ToString())
                return MetricDiameter.millimeter;

            throw new ArgumentOutOfRangeException("diameter");
        }

        private MetricWidth ConvertMetricWidth(string width)
        {
            if (width == MetricWidth.centimeter.ToString())
                return MetricWidth.centimeter;
            if (width == MetricWidth.kilometer.ToString())
                return MetricWidth.kilometer;
            if (width == MetricWidth.meter.ToString())
                return MetricWidth.meter;
            if (width == MetricWidth.millimeter.ToString())
                return MetricWidth.millimeter;

            throw new ArgumentOutOfRangeException("width");
        }

        private MetricHeight ConvertMetricHeight(string height)
        {
            if (height == MetricHeight.centimeter.ToString())
                return MetricHeight.centimeter;
            if (height == MetricHeight.kilometer.ToString())
                return MetricHeight.kilometer;
            if (height == MetricHeight.meter.ToString())
                return MetricHeight.meter;
            if (height == MetricHeight.millimeter.ToString())
                return MetricHeight.millimeter;

            throw new ArgumentOutOfRangeException("height");
        }

        private MetricVelocity ConvertMetricVelocity(string velocity)
        {
            if (velocity == MetricVelocity.kilometersPerHour.ToString())
                return MetricVelocity.kilometersPerHour;
            if (velocity == MetricVelocity.metersPerSecond.ToString())
                return MetricVelocity.metersPerSecond;

            throw new ArgumentOutOfRangeException("velocity");
        }

        private MetricFlow ConvertMetricFlow(string flow)
        {
            if (flow == MetricFlow.cubicMeterSecond.ToString())
                return MetricFlow.cubicMeterSecond;
            if (flow == MetricFlow.literPerMinute.ToString())
                return MetricFlow.literPerMinute;
            if (flow == MetricFlow.literPerSecond.ToString())
                return MetricFlow.literPerSecond;

            throw new ArgumentOutOfRangeException("flow");
        }

        private ImperialArea ConvertImperialArea(string area)
        {
            if (area == ImperialArea.acre.ToString())
                return ImperialArea.acre;
            if (area == ImperialArea.squareFoot.ToString())
                return ImperialArea.squareFoot;
            if (area == ImperialArea.squareInch.ToString())
                return ImperialArea.squareInch;
            if (area == ImperialArea.squareMiles.ToString())
                return ImperialArea.squareMiles;
            throw new ArgumentOutOfRangeException("area");
        }

        private ImperialLinear ConvertImperialLinear(string linear)
        {
            if (linear == ImperialLinear.USSurveyFoot.ToString())
                return ImperialLinear.USSurveyFoot;
            if (linear == ImperialLinear.foot.ToString())
                return ImperialLinear.foot;
            if (linear == ImperialLinear.inch.ToString())
                return ImperialLinear.inch;
            if (linear == ImperialLinear.mile.ToString())
                return ImperialLinear.mile;
            throw new ArgumentOutOfRangeException("linear");
        }

        private ImperialVolume ConvertImperialVolume(string volume)
        {
            if (volume == ImperialVolume.IMP_gallon.ToString())
                return ImperialVolume.IMP_gallon;
            if (volume == ImperialVolume.US_gallon.ToString())
                return ImperialVolume.US_gallon;
            if (volume == ImperialVolume.acreFeet.ToString())
                return ImperialVolume.acreFeet;
            if (volume == ImperialVolume.cubicFeet.ToString())
                return ImperialVolume.cubicFeet;
            if (volume == ImperialVolume.cubicInch.ToString())
                return ImperialVolume.cubicInch;
            if (volume == ImperialVolume.cubicYard.ToString())
                return ImperialVolume.cubicYard;

            throw new ArgumentOutOfRangeException("volume");
        }

        private ImperialTemperature ConvertImperialTemperature(string temperature)
        {
            if (temperature == ImperialTemperature.fahrenheit.ToString())
                return ImperialTemperature.fahrenheit;
            if (temperature == ImperialTemperature.kelvin.ToString())
                return ImperialTemperature.kelvin;

            throw new ArgumentOutOfRangeException("temperature");
        }

        private ImperialPressure ConvertImperialPressure(string pressure)
        {
            if (pressure == ImperialPressure.inHG.ToString())
                return ImperialPressure.inHG;
            if (pressure == ImperialPressure.inchHG.ToString())
                return ImperialPressure.inchHG;

            throw new ArgumentOutOfRangeException("pressure");
        }

        private ImperialDiameter ConvertImperialDiameter(string diameter)
        {
            if (diameter == ImperialDiameter.USSurveyFoot.ToString())
                return ImperialDiameter.USSurveyFoot;
            if (diameter == ImperialDiameter.foot.ToString())
                return ImperialDiameter.foot;
            if (diameter == ImperialDiameter.inch.ToString())
                return ImperialDiameter.inch;

            throw new ArgumentOutOfRangeException("diameter");
        }

        private ImperialWidth ConvertImperialWidth(string width)
        {
            if (width == ImperialWidth.USSurveyFoot.ToString())
                return ImperialWidth.USSurveyFoot;
            if (width == ImperialWidth.foot.ToString())
                return ImperialWidth.foot;
            if (width == ImperialWidth.inch.ToString())
                return ImperialWidth.inch;

            throw new ArgumentOutOfRangeException("width");
        }

        private ImperialHeigth ConvertImperialHeigth(string height)
        {
            if (height == ImperialHeigth.USSurveyFoot.ToString())
                return ImperialHeigth.USSurveyFoot;
            if (height == ImperialHeigth.foot.ToString())
                return ImperialHeigth.foot;
            if (height == ImperialHeigth.inch.ToString())
                return ImperialHeigth.inch;

            throw new ArgumentOutOfRangeException("height");
        }

        private ImperialVelocity ConvertImperialVelocity(string velocity)
        {
            if (velocity == ImperialVelocity.feetPerSecond.ToString())
                return ImperialVelocity.feetPerSecond;
            if (velocity == ImperialVelocity.milesPerHour.ToString())
                return ImperialVelocity.milesPerHour;

            throw new ArgumentOutOfRangeException("velocity");
        }

        private ImperialFlow ConvertImperialFlow(string flow)
        {
            if (flow == ImperialFlow.IMP_gallonPerDay.ToString())
                return ImperialFlow.IMP_gallonPerDay;
            if (flow == ImperialFlow.IMP_gallonPerMinute.ToString())
                return ImperialFlow.IMP_gallonPerMinute;
            if (flow == ImperialFlow.US_gallonPerDay.ToString())
                return ImperialFlow.US_gallonPerDay;
            if (flow == ImperialFlow.US_gallonPerMinute.ToString())
                return ImperialFlow.US_gallonPerMinute;
            if (flow == ImperialFlow.acreFeetDay.ToString())
                return ImperialFlow.acreFeetDay;
            if (flow == ImperialFlow.cubicFeetDay.ToString())
                return ImperialFlow.cubicFeetDay;
            if (flow == ImperialFlow.cubicFeetSecond.ToString())
                return ImperialFlow.cubicFeetSecond;

            throw new ArgumentOutOfRangeException("flow");
        }

        private ElevationType ConvertElevationType(string elevation)
        {
            if (elevation == ElevationType.feet.ToString())
                return ElevationType.feet;
            if (elevation == ElevationType.kilometer.ToString())
                return ElevationType.kilometer;
            if (elevation == ElevationType.meter.ToString())
                return ElevationType.meter;
            if (elevation == ElevationType.miles.ToString())
                return ElevationType.miles;
            throw new ArgumentOutOfRangeException("elevation");
        }

        private CurveType ConvertCurveType(string curve)
        {
            if (curve == CurveType.arc.ToString())
                return CurveType.arc;
            if (curve == CurveType.chord.ToString())
                return CurveType.chord;
            throw new ArgumentOutOfRangeException("curve");
        }
        #endregion


    }
}