﻿using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model
{
    /// <summary>
    /// A Single Alignment PI Definition
    /// </summary>
    public interface IAlignPi
    {
        #region XSD Documentation 1.2
        //<xs:element name="AlignPI">
        //  <xs:annotation>
        //    <xs:documentation>A Single Alignment PI Definition</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:choice maxOccurs="unbounded">
        //      <xs:element ref="Station"/>
        //      <xs:element ref="PI"/>
        //      <xs:element ref="InSpiral" minOccurs="0"/>
        //      <xs:element ref="Curve1" minOccurs="0"/>
        //      <xs:element ref="ConnSpiral" minOccurs="0"/>
        //      <xs:element ref="Curve2" minOccurs="0"/>
        //      <xs:element ref="OutSpiral" minOccurs="0"/>
        //    </xs:choice>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        double Station { get; set; }
        IPointTypeElement Pi { get; set; }
        #endregion

        #region Optional

        #endregion
    }
}
