﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LANDXMLRW.Enumerations;

namespace LANDXMLRW.Model.Alignment
{
    public class CantStation : ICantStation
    {

        public double Station { get; set; }

        public double AppliedCant { get; set; }

        public ClockWise Curvature { get; set; }

        public double EquilibriumCant { get; set; }

        public double CantDeficiency { get; set; }

        public double CantExcess { get; set; }

        public double RateOfChangeOfAppliedCantOverTime { get; set; }

        public double RateOfChangeOfAppliedCantOverLength { get; set; }

        public double RateOfChangeOfCantDeficiencyOverTime { get; set; }

        public double CantGradient { get; set; }

        public double Speed { get; set; }

        public SpiralType TransitionType { get; set; }

        public bool Adverse { get; set; }
    }
}
