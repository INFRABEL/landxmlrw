﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricDiameter
    {
        // ReSharper disable InconsistentNaming
        millimeter,
        centimeter,
        meter,
        kilometer
        // ReSharper restore InconsistentNaming
    }
}
