﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.GoordGeom
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICurveElement : IGeoElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Curve">
        //<xs:annotation>
        //<xs:documentation>
        //The distance from the Start to the Center provides the radius value.
        //</xs:documentation>
        //<xs:documentation>
        //The rotation attribute "rot" defines whether the arc travels clockwise or counter-clockwise from the Start to End point.
        //</xs:documentation>
        //</xs:annotation>
        //<xs:complexType>
        //<xs:choice minOccurs="3" maxOccurs="unbounded">
        //<xs:element ref="Start"/>
        //<xs:element ref="Center"/>
        //<xs:element ref="End"/>
        //<xs:element ref="PI" minOccurs="0"/>
        //<xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //</xs:choice>
        //<xs:attribute name="rot" type="clockwise" use="required"/>
        //<xs:attribute name="chord" type="xs:double"/>
        //<xs:attribute name="crvType" type="curveType"/>
        //<xs:attribute name="delta" type="angle"/>
        //<xs:attribute name="desc" type="xs:string"/>
        //<xs:attribute name="dirEnd" type="direction"/>
        //<xs:attribute name="dirStart" type="direction"/>
        //<xs:attribute name="external" type="xs:double"/>
        //<xs:attribute name="length" type="xs:double"/>
        //<xs:attribute name="midOrd" type="xs:double"/>
        //<xs:attribute name="name" type="xs:string"/>
        //<xs:attribute name="radius" type="xs:double"/>
        //<xs:attribute name="staStart" type="xs:double"/>
        //<xs:attribute name="state" type="stateType"/>
        //<xs:attribute name="tangent" type="xs:double"/>
        //<xs:attribute name="oID" type="xs:string"/>
        //<xs:attribute name="note" type="xs:string"/>
        //</xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        /// <summary>
        /// The rotation attribute "rot" defines whether the arc travels clockwise or counter-clockwise from the Start to End point.
        /// </summary>
        ClockWise Rotation { get; set; }
        #endregion

        #region Optional
        /// <summary>
        /// 
        /// </summary>
        double Chord { get; set; }
        /// <summary>
        /// 
        /// </summary>
        CurveType CurveType { get; set; }
        /// <summary>
        /// Represents a normalized angular value in the specified Angular units. Assume 0 degrees = east
        /// </summary>
        double Delta { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Represents a normalized angular value that indicates a horizontal direction, expressed in the specified Direction units. Assume 0 degrees = north
        /// </summary>
        double DirectionEnd { get; set; }
        /// <summary>
        /// Represents a normalized angular value that indicates a horizontal direction, expressed in the specified Direction units. Assume 0 degrees = north
        /// </summary>
        double DirectionStart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double External { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double Length { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double MidOrd { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// Distance from the start to the center provides the radius.
        /// </summary>
        double Radius { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double StationStart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double StationEnd { get; }
        /// <summary>
        /// 
        /// </summary>
        StateType? State { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double Tangent { get; set; }
        /// <summary>
        /// Official ID
        /// </summary>
        string OId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Note { get; set; }
        #endregion

        #region No Idea => To be verified
        /// <summary>
        /// Represents a 2D or 3D Center Point
        /// </summary>
        IPointTypeElement Center { get; set; }
        /// <summary>
        /// Represents a 2D or 3D Starting Point
        /// </summary>
        IPointTypeElement Start { get; set; }
        /// <summary>
        /// Represents a 2D or 3D Ending Point
        /// </summary>
        IPointTypeElement End { get; set; }
        /// <summary>
        /// Represents a 2D or 3D Point Of Intersection
        /// </summary>
        IPointTypeElement Pi { get; set; }
        /// <summary>
        /// Used to include additional information that is not explicitly defined by the LandXML schema
        /// Feature may contain one or more Property, DocFileRef or nested Feature Elements. 
        /// Note: to allow any valid content, the explicit definitions for Property, DocFileRef and Feature habe been commented out, but are still excpected in common use.
        /// </summary>
        IList<IFeatureElement> Features { get; set; }
        #endregion
    }
}
