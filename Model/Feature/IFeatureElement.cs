﻿using System.Collections.Generic;

namespace LANDXMLRW.Model.Feature
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFeatureElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Feature">
        //  <xs:annotation>
        //    <xs:documentation>Used to include additional information that is not explicitly defined by the LandXML schema, Feature may contain one or more Property, DocFileRef or nested Feature elements. 
        //NOTE: to allow any valid content, the explicit definitions for Property, DocFileRef and Feature have been commented out, but are still expected in common use.</xs:documentation>
        //    <xs:documentation>Each Property element defines one piece of data.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:element ref="Property" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="DocFileRef" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //      <!--	<xs:any namespace="##any" processContents="skip" minOccurs="0" maxOccurs="unbounded"/>
        //-->
        //    </xs:sequence>
        //    <xs:attribute name="name" type="xs:string" use="optional"/>
        //    <xs:attribute name="code" type="xs:string"/>
        //    <xs:attribute name="source" use="optional"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        #endregion

        #region Optional
        /// <summary>
        /// 
        /// </summary>
        IList<IFeatureElement> Features { get; set; }
        IList<IDocFileRefElement> DocFileReferences { get; set; }
        IList<IPropertyElement> Properties { get; set; } 
        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Code { get; set; }
        //ToDo: What the fuck is the source all abouth?
        /// <summary>
        /// 
        /// </summary>
        string Source { get; set; }
        #endregion
    }
}
