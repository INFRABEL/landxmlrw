﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.Profile
{
    public class ProfAlign : IProfAlign
    {
        private IList<IPointTypeElement> _pvi = new List<IPointTypeElement>();
        public IList<IPointTypeElement> Pvi
        {
            get { return _pvi; }
            set { _pvi = value; }
        }

        private IList<IParaCurve> _paraCurves = new List<IParaCurve>(); 
        public IList<IParaCurve> ParaCurves
        {
            get { return _paraCurves; }
            set
            {
                _paraCurves = value;
            }
        }

        private IList<IUnsymParaCurve> _unsymParaCurves = new List<IUnsymParaCurve>();
        public IList<IUnsymParaCurve> UnsymParaCurves
        {
            get { return _unsymParaCurves; }
            set
            {
               _unsymParaCurves = value;
            }
        }

        private IList<ICircCurve> _circCurves = new List<ICircCurve>();
        public IList<ICircCurve> CircCurves
        {
            get { return _circCurves; }
            set { _circCurves = value; }
        }

        private IList<IFeatureElement> _features = new List<IFeatureElement>();
        public IList<IFeatureElement> Features
        {
            get
            {
                return _features;
            }
            set { _features = value; }
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public StateType? State { get; set; }
    }
}
