﻿using System.Collections.Generic;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model
{
    /// <summary>
    ///  Simplified coordinate systems definitions to reuse work done by
    ///EPSG (European Petroleum Survey Group)
    ///EPSG Code: EPSG has reserved the integer range 0 to 32767 for use as codes for coordinate systems.
    ///Example: Represents Australian Map Grid Zone 52
    ///name="AGD66 - AMG Zone 52" , epsgCode="20252" 
    ///Example: Represents Colorado CS27 South Zone
    ///name="NAD27-Colorado South" , epsgCode="26755" 
    /// </summary>
    public interface ICoordinateSystemElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="CoordinateSystem">
        //  <xs:annotation>
        //    <xs:documentation>
        //    Simplified coordinate systems definitions to reuse work done by
        //EPSG (European Petroleum Survey Group)
        //EPSG Code: EPSG has reserved the integer range 0 to 32767 for use as codes for coordinate systems.
        //                    Example: Represents Australian Map Grid Zone 52
        //                     name="AGD66 - AMG Zone 52" , epsgCode="20252" 

        //                    Example: Represents Colorado CS27 South Zone
        //                     name="NAD27-Colorado South" , epsgCode="26755" 
        //</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:element ref="Start" minOccurs="0"/>
        //      <xs:element ref="FieldNote" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:any namespace="##other" processContents="skip" minOccurs="0"/>
        //    </xs:sequence>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="name" type="xs:string"/>
        //    <xs:attribute name="epsgCode" type="xs:string"/>
        //    <xs:attribute name="ogcWktCode" type="xs:string"/>
        //    <xs:attribute name="horizontalDatum" type="xs:string"/>
        //    <xs:attribute name="verticalDatum" type="xs:string"/>
        //    <xs:attribute name="ellipsoidName" type="xs:string"/>
        //    <xs:attribute name="horizontalCoordinateSystemName" type="xs:string"/>
        //    <xs:attribute name="geocentricCoordinateSystemName" type="xs:string"/>
        //    <xs:attribute name="fileLocation" type="xs:anyURI"/>
        //    <xs:attribute name="rotationAngle" type="angle"/>
        //    <xs:attribute name="datum" type="xs:string"/>
        //    <xs:attribute name="fittedCoordinateSystemName" type="xs:string"/>
        //    <xs:attribute name="compoundCoordinateSystemName" type="xs:string"/>
        //    <xs:attribute name="localCoordinateSystemName" type="xs:string"/>
        //    <xs:attribute name="geographicCoordinateSystemName" type="xs:string"/>
        //    <xs:attribute name="projectedCoordinateSystemName" type="xs:string"/>
        //    <xs:attribute name="verticalCoordinateSystemName" type="xs:string"/>
        //    <!--  The attributes below are provided for backward compatibility only and should no longer be used. -->
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        #endregion

        #region Optional
        //Todo: Add Start
        //Todo:Add FieldNote
        string Description { get; set; }
        string Name { get; set; }
        string EpsgCode { get; set; }
        string OgcWktCode { get; set; }
        string HorizontalDatum { get; set; }
        string VerticalDatum { get; set; }
        string EllipsoidName { get; set; }
        string HorizontalCoordinateSystemName { get; set; }
        string GeocentricCoordinateSystemName { get; set; }
        string FileLocation { get; set; }
        double RotationAngle { get; set; }
        string Datum { get; set; }
        string FittedCoordinateSystemName { get; set; }
        string CompoundCoordinateSystemName { get; set; }
        string GeographicCoordinateSystemName { get; set; }
        string ProjectedCoordinateSystemName { get; set; }
        string VerticalCoordinateSystemName { get; set; }
        string LocalCoordinateSystemName { get; set; }

        IList<IFeatureElement> Features { get; set; } 
        #endregion
    }
}
