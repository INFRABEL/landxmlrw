﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialVelocity
    {
        // ReSharper disable InconsistentNaming
        feetPerSecond,
        milesPerHour
        // ReSharper restore InconsistentNaming
    }
}
