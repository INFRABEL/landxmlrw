﻿namespace LANDXMLRW.Model.Geometry
{
    public class Point3DOptElement : IPointTypeElement
    {
        public double X { get; set; }

        public double Y { get; set; }

        public double Z { get; set; }
    }
}
