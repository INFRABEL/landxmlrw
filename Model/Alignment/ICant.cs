﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model.Alignment
{
    /// <summary>
    /// The "Cant" element will typically represent a proposed railway cant / superelevation alignment.
    /// It is defined by a sequential series of any combination of the cant stations and speed-only stations.
    /// </summary>
    public interface ICant
    {
        #region XML Documentation 1.2
        //<xs:element name="Cant">
        //  <xs:annotation>
        //    <xs:documentation>The "Cant" element will typically represent a proposed railway cant / superelevation alignment.</xs:documentation>
        //    <xs:documentation>It is defined by a sequential series of any combination of the cant stations and speed-only stations.
        //The “name”, “desc” and “state” attributes are typical LandXML “alignment” attributes.
        //The “equilibriumConstant” is a unitless optional double that is used as the equilibrium constant in the cant equilibrium equation (cant = constant * speed * speed / radius).
        //The “appliedCantConstant” is a unitless optional double that is used as the applied cant constant in the cant equilibrium equation (cant = constant * speed * speed / radius).
        //The “gauge” is a required double that is the rail to rail distance.  This value is expressed in meters or feet depending upon the units.
        //The “rotationPoint” is an optional string that defines the rotation point.  Valid values are “insideRail”, “outsideRail”, “center”, “leftRail” and “rightRail”.
        //</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:sequence>
        //      <xs:choice maxOccurs="unbounded">
        //        <xs:element ref="CantStation" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:element ref="SpeedStation" minOccurs="0" maxOccurs="unbounded"/>
        //      </xs:choice>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //    </xs:sequence>
        //    <xs:attribute name="name" type="xs:string" use="required"/>
        //    <xs:attribute name="desc" type="xs:string" use="optional"/>
        //    <xs:attribute name="state" type="stateType" use="optional"/>
        //    <xs:attribute name="equilibriumConstant" type="xs:double" use="optional"/>
        //    <xs:attribute name="appliedCantConstant" type="xs:double" use="optional"/>
        //    <xs:attribute name="gauge" type="xs:double" use="required"/>
        //    <xs:attribute name="rotationPoint" type="xs:string" use="optional"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Choice
        IList<ICantStation> CantStations { get; set; }
        IList<ISpeedStation> SpeedStations { get; set; } 
        #endregion

        #region Required
        /// <summary>
        /// The “name” attribute is a typical LandXML “alignment” attribute.
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// The “gauge” is a required double that is the rail to rail distance.  This value is expressed in meters or feet depending upon the units.
        /// </summary>
        double Gauge { get; set; }
        #endregion

        #region Optional
        /// <summary>
        /// The “desc” attribute is a typical LandXML “alignment” attribute.
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// The “state” attribute is a typical LandXML “alignment” attribute.
        /// </summary>
        StateType State { get; set; }
        /// <summary>
        /// The “equilibriumConstant” is a unitless optional double that is used as the equilibrium constant in the cant equilibrium equation (cant = constant * speed * speed / radius).
        /// </summary>
        double EquilibriumConstant { get; set; }
        /// <summary>
        /// The “appliedCantConstant” is a unitless optional double that is used as the applied cant constant in the cant equilibrium equation (cant = constant * speed * speed / radius).
        /// </summary>
        double AppliedCantConstant { get; set; }
        /// <summary>
        /// The “rotationPoint” is an optional string that defines the rotation point.  Valid values are “insideRail”, “outsideRail”, “center”, “leftRail” and “rightRail”.
        /// </summary>
        string RotationPoint { get; set; }
        IList<IFeatureElement> Features { get; set; }
        #endregion
    }
}
