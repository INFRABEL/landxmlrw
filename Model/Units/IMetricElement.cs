﻿using LANDXMLRW.Enumerations.Units;
using LANDXMLRW.Enumerations.Units.Metric;

namespace LANDXMLRW.Model.Units
{
    public interface IMetricElement : IUnitElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Metric">
        //  <xs:annotation>
        //    <xs:documentation/>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:attribute name="areaUnit" type="metArea" use="required"/>
        //    <xs:attribute name="linearUnit" type="metLinear" use="required"/>
        //    <xs:attribute name="volumeUnit" type="metVolume" use="required"/>
        //    <xs:attribute name="temperatureUnit" type="metTemperature" use="required"/>
        //    <xs:attribute name="pressureUnit" type="metPressure" use="required"/>
        //    <xs:attribute name="diameterUnit" type="metDiameter"/>
        //    <xs:attribute name="widthUnit" type="metWidth"/>
        //    <xs:attribute name="heightUnit" type="metHeight"/>
        //    <xs:attribute name="velocityUnit" type="metVelocity"/>
        //    <xs:attribute name="flowUnit" type="metFlow"/>
        //    <xs:attribute name="angularUnit" type="angularType" default="radians"/>
        //    <xs:attribute name="directionUnit" type="angularType" default="radians"/>
        //    <xs:attribute name="latLongAngularUnit" type="latLongAngularType" default="decimal degrees"/>
        //    <xs:attribute name="elevationUnit" type="elevationType" default="meter"/>
        //    <!-- FAA Additions start -->
        //    <!-- FAA Additions end -->
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        MetricArea AreaUnit { get; set; }
        MetricLinear LinearUnit { get; set; }
        MetricVolume VolumeUnit { get; set; }
        MetricTemperature TemperatureUnit { get; set; }
        MetricPressure PressureUnit { get; set; }
        #endregion

        #region Optional
        MetricDiameter? DiameterUnit { get; set; }
        MetricWidth? WidthUnit { get; set; }
        MetricHeight? HeightUnit { get; set; }
        MetricVelocity? VelocityUnit { get; set; }
        MetricFlow? FlowUnit { get; set; }
        string AngularUnit { get; set; }
        string DirectionUnit { get; set; }
        string LatLongAngularUnit { get; set; }
        ElevationType? ElevationUnit { get; set; }
        #endregion
    }
}
