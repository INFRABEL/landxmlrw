﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LANDXMLRW.Model
{
    public class AuthorElement : IAuthorElement
    {
        public string CreatedBy { get; set; }

        public string Email { get; set; }

        public string Company { get; set; }

        public string CompanyUrl { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}
