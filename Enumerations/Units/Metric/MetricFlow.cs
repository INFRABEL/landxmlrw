﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricFlow
    {
        // ReSharper disable InconsistentNaming
        cubicMeterSecond,
        literPerSecond,
        literPerMinute
        // ReSharper restore InconsistentNaming
    }
}
