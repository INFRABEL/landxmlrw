﻿using System.Collections.Generic;

namespace LANDXMLRW.Model
{
    public class CoordinateSystem : ICoordinateSystemElement
    {
        public string Description { get; set; }

        public string Name { get; set; }

        public string EpsgCode { get; set; }

        public string OgcWktCode { get; set; }

        public string HorizontalDatum { get; set; }

        public string VerticalDatum { get; set; }

        public string EllipsoidName { get; set; }

        public string HorizontalCoordinateSystemName { get; set; }

        public string GeocentricCoordinateSystemName { get; set; }

        public string FileLocation { get; set; }

        public double RotationAngle { get; set; }

        public string Datum { get; set; }

        public string FittedCoordinateSystemName { get; set; }

        public string CompoundCoordinateSystemName { get; set; }

        public string GeographicCoordinateSystemName { get; set; }

        public string ProjectedCoordinateSystemName { get; set; }

        public string VerticalCoordinateSystemName { get; set; }

        public string LocalCoordinateSystemName { get; set; }

        public IList<Feature.IFeatureElement> Features { get; set; }
    }
}
