﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialLinear
    {
        // ReSharper disable InconsistentNaming
        foot,
        USSurveyFoot,
        inch,
        mile
        // ReSharper restore InconsistentNaming
    }
}
