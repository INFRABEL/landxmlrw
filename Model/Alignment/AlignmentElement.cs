﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;
using LANDXMLRW.Model.GoordGeom;
using LANDXMLRW.Model.Profile;

namespace LANDXMLRW.Model.Alignment
{
    public class AlignmentElement : IAlignmentElement
    {
        public IPointTypeElement Start { get; set; }

        public IList<IGeoElement> CoordGeom { get; set; }

        public string Name { get; set; }

        public double Length { get; set; }

        public double StaStart { get; set; }

        public string Description { get; set; }

        public string OId { get; set; }

        public StateType? State { get; set; }

        public IList<IFeatureElement> Features { get; set; }

        public IList<IPointTypeElement> PIs { get; set; }

        public ICant Cant { get; set; }

        public IProfile Profile { get; set; }

        public AlignmentElement()
        {
            Profile = new Profile.Profile();
            Cant = new Cant();
            PIs = new List<IPointTypeElement>();
            Features = new List<IFeatureElement>();
            CoordGeom = new List<IGeoElement>();
        }


    }
}
