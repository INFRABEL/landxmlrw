﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricPressure
    {
        // ReSharper disable InconsistentNaming
        HPA,
        milliBars,
        mmHG,
        millimeterHG
        // ReSharper restore InconsistentNaming
    }
}
