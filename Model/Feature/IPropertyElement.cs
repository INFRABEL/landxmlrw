﻿namespace LANDXMLRW.Model.Feature
{
    /// <summary>
    /// Used to include additional information that is not explicitly defined by the LandXML schema. Each Property element defines one piece of data
    /// 
    /// </summary>
    public interface IPropertyElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Property">
        //  <xs:annotation>
        //    <xs:documentation>Used to include additional information that is not explicitly defined by the LandXML schema. Each Property element defines one piece of data.</xs:documentation>
        //    <xs:documentation>The "label" attribute defines the name of the value held in the "value" attribute.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:attribute name="label" use="required"/>
        //    <xs:attribute name="value" use="required"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        /// <summary>
        /// The "label" attribute defines the name of the value held in the "value" attribute.
        /// </summary>
        string Label { get; set; }
        string Value { get; set; }
        #endregion
    }
}
