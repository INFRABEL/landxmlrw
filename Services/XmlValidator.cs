﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace LANDXMLRW.Services
{
    public class XmlValidator
    {
        #region Property's

        /// <summary>
        /// Path to the XSD file
        /// </summary>
        public string XsdPath { get; set; }

        /// <summary>
        /// Path to the XML file
        /// </summary>
        public string XmlPath { get; set; }

        /// <summary>
        /// This stringbuilder holds the log.
        /// </summary>
        public StringBuilder Log
        {
            get;
            private set;
        }

        /// <summary>
        /// All exceptions that are found when validating the XML-File will be stored here.
        /// </summary>
        public IList<Exception> XmlExceptions
        {
            get;
            private set;
        }

        #endregion Property's

        /// <summary>
        /// Creates an instance of the XmlValidator class.
        /// </summary>
        /// <param name="xmlPath">Path to the xml file.</param>
        /// <param name="xsdPath">Path to the xsd file.</param>
        public XmlValidator(string xmlPath, string xsdPath = "")
        {
            XmlPath = xmlPath;
            XsdPath = xsdPath;
        }

        /// <summary>
        ///     This function will validate an XML file for conformity to the XSD
        ///     schema provided. If the xsd file is not provided, it will try to resolve it
        ///     by using the Uri found in the XML file. Know that this process requires a
        ///     valid internet connection, and is a lot slower than actually providing
        ///     the xsd file from disk.
        ///     When validation has been completed, the XmlExceptions and Log property's can be
        ///     used to trace back any exceptions that have occured during the validation process.
        /// </summary>
        /// <param name="xsdPath">Optional: Path to the xsd file</param>
        /// <param name="xmlPath">Optional path to the xml file. If this has been set already with constructor, or by using the setter, u can omit this.</param>
        /// <exception cref="ArgumentNullException">Throw argument null exeption if the xsd/xml path has not been set by overloads, or property setter.</exception>
        /// <returns>True if success, false if validation failed.</returns>
        /// <exception cref="IOException">When the XSD file is located on disk, and there are exceptions while trying to access this file, an excepion shall be thrown.</exception>
        public bool Validate(string xmlPath = "", string xsdPath = "")
        {
            // An argument needs to be provided if no path has been set on the property.
            if (string.IsNullOrWhiteSpace(xmlPath) && string.IsNullOrWhiteSpace(XmlPath))
                throw new ArgumentNullException("xmlPath");
            if (!string.IsNullOrWhiteSpace(xmlPath))
                XmlPath = xmlPath;

            // When the overload has been used, we need to fill it in into the property.
            if (!string.IsNullOrWhiteSpace(xsdPath))
                XsdPath = xsdPath;

            // Initialize property's
            Log = new StringBuilder();
            XmlExceptions = new List<Exception>();

            // Initialize the local variables
            var xmlReaderSettings = new XmlReaderSettings();
            var xmlSchemaSet = new XmlSchemaSet();
            try
            {
                // If an XsdPath has been provided
                XmlSchema xmlSchema = null;
                if (!string.IsNullOrWhiteSpace(XsdPath))
                {
                    try
                    {
                        xmlSchema = XmlSchema.Read(reader: new StreamReader(path: XsdPath), validationEventHandler: null);
                    }
                    catch (ArgumentException argumentException)
                    {
                        // If the path of the xsd is empty, we would never get into this if statement, so catching should not be an issue.
                        Debug.WriteLine(
                            "An empty-or null string has been provided as XSD path. Parameter name: {0}, message: {1}",
                            argumentException.ParamName, argumentException.Message);
                    }
                    catch (IOException ioException)
                    {
                        // This means there has been an error while trying to access the file on disk.
                        Debug.WriteLine(
                            "There was an error thrown while trying to read the file from disk. message: {0}", ioException.Message);
                        throw;
                    }

                    if (xmlSchema != null)
                    {
                        xmlSchemaSet.Add(schema: xmlSchema);
                        xmlSchemaSet.CompilationSettings.EnableUpaCheck = true;
                        xmlSchemaSet.Compile();
                    }
                }

                xmlReaderSettings.ValidationType = ValidationType.Schema;
                xmlReaderSettings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                xmlReaderSettings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;
                xmlReaderSettings.ConformanceLevel = ConformanceLevel.Document;
                xmlReaderSettings.ValidationEventHandler += ValidationEventHandler;

                // If no xsd file has been compled (provided) the xsd needs to be resolved
                // from the actual Uri found in the xml file.
                if (xmlSchemaSet.IsCompiled == false)
                {
                    xmlReaderSettings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
                }
                else
                {
                    xmlReaderSettings.Schemas = xmlSchemaSet;
                }

                try
                {
                    using (var xmlReader = XmlReader.Create(XmlPath, xmlReaderSettings))
                    {
                        var xmlDocument = new XmlDocument();
                        try
                        {
                            xmlDocument.Load(xmlReader);
                        }
                        catch (XmlException xmlException)
                        {
                            Log.AppendFormat(
                                "Validation failed by a critical xmlexception: location of the XML-File {0}{1}{2}{1}",
                                XsdPath, Environment.NewLine, xmlException.Message);
                            XmlExceptions.Add(xmlException);
                            return false;
                        }

                        // If the document is null here, the stringbuilder will be empty, so we
                        // need to make sure we throw an exception about this.
                        if (xmlSchema != null &&
                            (xmlDocument.DocumentElement != null &&
                             !xmlDocument.DocumentElement.NamespaceURI.Equals(xmlSchema.TargetNamespace)))
                        {
                            Log.AppendFormat(
                                "The namespace specified in the XSD does not equal the name in the XML file{0}",
                                Environment.NewLine);

                            if (string.IsNullOrEmpty(xmlDocument.DocumentElement.NamespaceURI) &&
                                string.IsNullOrEmpty(xmlSchema.TargetNamespace))
                            {
                                Log.AppendFormat(
                                    "The XML-file did not have a namespaceURI in it.{0}The XSD-file did not have a target namespace in it.{0}This is no best practice and can lead to incorrect validation results!{0}",
                                    Environment.NewLine);
                            }
                            else if (string.IsNullOrEmpty(xmlDocument.DocumentElement.NamespaceURI))
                            {
                                Log.AppendFormat(
                                    "The XML-file did not have a target namespace in it.{0}This is no best practice and can lead to incorrect validation results!{0}",
                                    Environment.NewLine);
                            }
                            else if (string.IsNullOrEmpty(xmlSchema.TargetNamespace))
                            {
                                Log.AppendFormat(
                                    "The XSD-file did not have a namespace in it.{0}This is no best practice and can lead to incorrect validation results!{0}Schema target has temperately been renamed to doc target{0}",
                                    Environment.NewLine);
                            }

                            if (string.IsNullOrEmpty(xmlDocument.DocumentElement.NamespaceURI))
                                Log.Append("NameSpaceUri from the XML DOC: NO NAMESPACE IN FILE!!!");
                            else
                                Log.AppendFormat("NameSpaceUri from the XML DOC:{0}",
                                    xmlDocument.DocumentElement.NamespaceURI);
                            Log.Append(Environment.NewLine);

                            if (string.IsNullOrEmpty(xmlSchema.TargetNamespace))
                                Log.Append("TargetNamespace from the XSD DOC: NO TARGET NAMESPACE IN FILE!!!");
                            else
                                Log.AppendFormat(
                                    "TargetNamespace from the XSD DOC:{0}{1}Make sure you use the right SCHEMA VERSION for a specified XML-file!!{1}",
                                    xmlSchema.TargetNamespace, Environment.NewLine);
                            //http://www.w3.org/XML/
                            Log.Append("For more information visit http://www.w3.org/XML/ ");
                            Log.Append(Environment.NewLine);
                        }
                    }
                }
                catch (ArgumentNullException argumentNullException)
                {
                    Debug.WriteLine("A null object has been provided as parameter. Parametername: {0}, message: {1}",
                        argumentNullException.ParamName, argumentNullException.Message);
                    // When this happens we really want to throw this problem up the exception chain.
                    throw;
                }
                catch (FileNotFoundException fileNotFoundException)
                {
                    // this could be a commen error and we can handle it here, if the xml file can not be found our validation can be canceled.
                    Log.AppendFormat("Validation failed: xml file could not be found.");
                    XmlExceptions.Add(fileNotFoundException);
                    return false;
                }
                catch (UriFormatException uriFormatException)
                {
                    Log.AppendFormat("Validation failed: invalid Uri format has been provided: info: {0}", uriFormatException.Message);
                    XmlExceptions.Add(uriFormatException);
                    return false;
                }
            }
            catch (XmlSchemaException xmlSchemaException)
            {
                Log.AppendFormat("Validation failed by a critical xmlSchemaException: location of the XML-File {0}{1}{2}{1}", XsdPath, Environment.NewLine, xmlSchemaException.Message);
                XmlExceptions.Add(xmlSchemaException);
                return false;
            }

            if (string.IsNullOrWhiteSpace(Log.ToString()))
            {
                Log.AppendFormat("Validation completed successfully of XML-File {0}", XmlPath);
                return true;
            }
            Log.AppendFormat("Validation failed: location of the XML-File {0}{1}", XsdPath, Environment.NewLine);
            return false;
        }

        #region Private method's and logic

        private void ValidationEventHandler(object sender, ValidationEventArgs args)
        {
            switch (args.Severity)
            {
                case XmlSeverityType.Error:
                    {
                        // Make sure we can still access the exceptions from a property in this class.
                        XmlExceptions.Add(args.Exception);
                        Log.AppendFormat("Validation error: [LINE: {0}| POS: {1} ] {2}{3}{2}{2}", args.Exception.LineNumber, args.Exception.LinePosition, Environment.NewLine, args.Exception.Message);
                        break;
                    }

                case XmlSeverityType.Warning:
                    {
                        // Make sure we can still access the exceptions from a property in this class.
                        XmlExceptions.Add(args.Exception);
                        Log.AppendFormat(
                            "Validation warning: [LINE: {0}| POS: {1}| Source Object: {2} ] {3}{4}{3}{3}",
                            args.Exception.LineNumber, args.Exception.LinePosition, args.Exception.SourceSchemaObject,
                            Environment.NewLine, args.Exception.Message);

                        break;
                    }

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion Private method's and logic
    }
}