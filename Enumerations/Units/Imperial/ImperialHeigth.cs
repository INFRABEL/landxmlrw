﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialHeigth
    {
        // ReSharper disable InconsistentNaming
        foot,
        USSurveyFoot,
        inch
        // ReSharper restore InconsistentNaming
    }
}
