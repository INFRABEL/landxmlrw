﻿using LANDXMLRW.Enumerations;

namespace LANDXMLRW.Model.Alignment
{
    public interface ICantStation
    {
        #region XML Documentation 1.2
        //<xs:element name="CantStation">
        //  <xs:annotation>
        //    <xs:documentation>A cant station.
        //            The “station” is a required double that is internal station value.
        //The “equilibriumCant” is an optional double that is the equilibrium cant.  This value is expressed in millimeters or inches depending upon the units
        //The “appliedCant” is a required double that is the applied cant.  This value is expressed in millimeters or inches depending upon the units.
        //The “deficiencyCant” is an optional double that is the cant deficiency.  This value is expressed in millimeters or inches depending upon the units.
        //The “cantExcess” is an optional double that is the cant excess.  This value is expressed in millimeters or inches upon the units.
        //The “rateOfChangeOfAppliedCantOverTime” is an optional double that is the rate of change of applied cant as a function of time.  This value is in millimeters /seconds or inches/seconds depending upon the units.
        //The “rateOfChangeOfAppliedCantOverLength” is an optional double that is the rate of change of applied cant as a function of length.  This value is in millimeters /meters or inches/feet depending upon the units.
        //The “rateOfChangeOfCantDeficiencyOverTime” is an optional double that is the rate of change of cant deficiency as a function of time.  This value is in millimeters /seconds or inches/seconds depending upon the units.
        //The “cantGradient” is an optional double that is the cant gradient.  This value is unitless.
        //The “speed” is an optional double that is the design speed.  This value is in kmph or mph depending upon the units.
        //The “transitionType” is an optional enumerated type.
        //The “curvature” is a required enumerated type.
        //The “adverse” is an optional Boolean that indicates whether the cant is adverse.
        //</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:attribute name="station" type="xs:double" use="required"/>
        //    <xs:attribute name="equilibriumCant" type="xs:double" use="optional"/>
        //    <xs:attribute name="appliedCant" type="xs:double" use="required"/>
        //    <xs:attribute name="cantDeficiency" type="xs:double" use="optional"/>
        //    <xs:attribute name="cantExcess" type="xs:double" use="optional"/>
        //    <xs:attribute name="rateOfChangeOfAppliedCantOverTime" type="xs:double" use="optional"/>
        //    <xs:attribute name="rateOfChangeOfAppliedCantOverLength" type="xs:double" use="optional"/>
        //    <xs:attribute name="rateOfChangeOfCantDeficiencyOverTime" type="xs:double" use="optional"/>
        //    <xs:attribute name="cantGradient" type="xs:double" use="optional"/>
        //    <xs:attribute name="speed" type="xs:double" use="optional"/>
        //    <xs:attribute name="transitionType" type="spiralType" use="optional"/>
        //    <xs:attribute name="curvature" type="clockwise" use="required"/>
        //    <xs:attribute name="adverse" type="xs:boolean" use="optional"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        double Station { get; set; }
        double AppliedCant { get; set; }
        ClockWise Curvature { get; set; }
        #endregion

        #region Optional
        double EquilibriumCant { get; set; }
        double CantDeficiency { get; set; }
        double CantExcess { get; set; }
        double RateOfChangeOfAppliedCantOverTime { get; set; }
        double RateOfChangeOfAppliedCantOverLength { get; set; }
        double RateOfChangeOfCantDeficiencyOverTime { get; set; }
        double CantGradient { get; set; }
        double Speed { get; set; }
        SpiralType TransitionType { get; set; }
        bool Adverse { get; set; }
        #endregion
    }
}
