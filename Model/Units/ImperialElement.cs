﻿using LANDXMLRW.Enumerations.Units;
using LANDXMLRW.Enumerations.Units.Imperial;

namespace LANDXMLRW.Model.Units
{
    public class ImperialElement : IImperialElement
    {
        public ImperialArea AreaUnit { get; set; }

        public ImperialLinear LinearUnit { get; set; }

        public ImperialVolume VolumeUnit { get; set; }

        public ImperialTemperature TemperatureUnit { get; set; }

        public ImperialPressure PressureUnit { get; set; }

        public ImperialDiameter? DiameterUnit { get; set; }

        public ImperialWidth? WidthUnit { get; set; }

        public ImperialHeigth? HeightUnit { get; set; }

        public ImperialVelocity? VelocityUnit { get; set; }

        public ImperialFlow? FlowUnit { get; set; }

        public string AngularUnit { get; set; }

        public string DirectionUnit { get; set; }

        public string LatLongAngularUnit { get; set; }

        public ElevationType? ElevationUnit { get; set; }
    }
}