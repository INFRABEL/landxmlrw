﻿namespace LANDXMLRW.Enumerations
{
    public enum StateType
    {
        // ReSharper disable InconsistentNaming
        abandoned,
        destroyed,
        existing,
        proposed
        // ReSharper restore InconsistentNaming
    }
}
