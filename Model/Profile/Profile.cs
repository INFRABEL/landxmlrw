﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model.Profile
{
    public class Profile : IProfile
    {
        private IList<IProfSurf> _profileSurfs = new List<IProfSurf>();
        public IList<IProfSurf> ProfileSurfs
        {
            get { return _profileSurfs; }
            set { _profileSurfs = value; }
        }

        private IList<IProfAlign> _profAligns = new List<IProfAlign>();
        public IList<IProfAlign> ProfAligns
        {
            get { return _profAligns; }
            set { _profAligns = value; }
        }

        private IList<IFeatureElement> _features = new List<IFeatureElement>(); 
        public IList<IFeatureElement> Features
        {
            get { return _features; }
            set { _features = value; }
        }

        public string Description { get; set; }

        public string Name { get; set; }

        public double StationStart { get; set; }

        public StateType State { get; set; }
    }
}
