﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialArea
    {
        // ReSharper disable InconsistentNaming
        acre,
        squareFoot,
        squareInch,
        squareMiles,
        // ReSharper restore InconsistentNaming
    }
}
