﻿namespace LANDXMLRW.Enumerations
{
    public enum ClockWise
    {
        // ReSharper disable InconsistentNaming
        cw,
        ccw
        // ReSharper restore InconsistentNaming
    }
}
