﻿namespace LANDXMLRW.Model.Geometry
{
    /// <summary>
    /// A text value that is a space delimited list of doubles. It is used as the base type to define point coordinates in the form of "northing easting" or "northing easting elevation" as well as point lists of 2D or 3D points with items such as surface boundaries or "station elevation", "station offset" lists for items such as profiles and cross sections: 
    ///Example, "1632.546 2391.045 240.30"
    /// </summary>
    public interface IPointElement
    {
        #region XSD Documentation 1.2
        //<xs:simpleType name="Point">
        //<xs:annotation>
        //    <xs:documentation>A text value that is a space delimited list of doubles. It is used as the base type to define point coordinates in the form of "northing easting" or "northing easting elevation" as well as point lists of 2D or 3D points with items such as surface boundaries or "station elevation", "station offset" lists for items such as profiles and cross sections: 
        //Example, "1632.546 2391.045 240.30"</xs:documentation>
        //  </xs:annotation>
        //  <xs:list itemType="xs:double"/>
        //</xs:simpleType>
        #endregion

        #region Required
        #endregion

        #region Optional
        #endregion

        double X { get; set; }
        double Y { get; set; }
        double Z { get; set; }
    }
}
