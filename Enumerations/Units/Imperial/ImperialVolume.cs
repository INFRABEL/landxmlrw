﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialVolume
    {
        // ReSharper disable InconsistentNaming
        US_gallon,
        IMP_gallon,
        cubicInch,
        cubicFeet,
        cubicYard,
        acreFeet
        // ReSharper restore InconsistentNaming
    }
}
