﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricVelocity
    {
        // ReSharper disable InconsistentNaming
        metersPerSecond,
        kilometersPerHour
        // ReSharper restore InconsistentNaming
    }
}
