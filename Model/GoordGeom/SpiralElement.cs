﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.GoordGeom
{
  public class SpiralElement : ISpiralElement
  {
    public double Length { get; set; }

    public double RadiusStart { get; set; }

    public double RadiusEnd { get; set; }

    public ClockWise Rotation { get; set; }

    public SpiralType SpiralType { get; set; }

    public double Chord { get; set; }

    public double Constant { get; set; }

    public string Description { get; set; }

    public double DirectionEnd { get; set; }

    public double DirectionStart { get; set; }

    public string Name { get; set; }

    public double Theta { get; set; }

    public double TotalX { get; set; }

    public double TotalY { get; set; }

    public double StationStart { get; set; }
        
    public double StationEnd{ get { return StationStart + Length; } }

    public StateType? State { get; set; }

    public double TanLong { get; set; }

    public double TanShort { get; set; }

    public string OId { get; set; }

    public IPointTypeElement Start { get; set; }

    public IPointTypeElement End { get; set; }

    public IPointTypeElement Pi { get; set; }

    public IList<Feature.IFeatureElement> Features { get; set; }
  }
}
