﻿using System;

namespace LANDXMLRW.Model
{
    public class ApplicationElement : IApplicationElement
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Manufacturer { get; set; }

        public string Version { get; set; }

        public string ManufacturerUrl { get; set; }

        public DateTime TimeStamp { get; set; }

        private IAuthorElement _author = new AuthorElement();
        public IAuthorElement Author
        {
            get { return _author; }
            set { _author = value; }
        }
    }
}
