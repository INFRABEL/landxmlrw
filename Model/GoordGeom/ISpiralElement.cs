﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.GoordGeom
{
    /// <summary>
    /// 
    /// </summary>
    public interface ISpiralElement : IGeoElement
    {
        #region XSD Documentation 1.2
        //        <xs:element name="Spiral">
        //<xs:annotation>
        //<xs:documentation>
        //An "infinite" spiral radius is denoted by the value "INF".
        //</xs:documentation>
        //<xs:documentation>
        //This conforms to XML Schema which defines infinity as "INF" or "-INF" for all numeric datatypes
        //</xs:documentation>
        //</xs:annotation>
        //<xs:complexType>
        //<xs:sequence>
        //<xs:choice minOccurs="3" maxOccurs="3">
        //<xs:element ref="Start"/>
        //<xs:element ref="PI"/>
        //<xs:element ref="End"/>
        //</xs:choice>
        //<xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //</xs:sequence>
        //<xs:attribute name="length" type="xs:double" use="required"/>
        //<xs:attribute name="radiusEnd" type="xs:double" use="required"/>
        //<xs:attribute name="radiusStart" type="xs:double" use="required"/>
        //<xs:attribute name="rot" type="clockwise" use="required"/>
        //<xs:attribute name="spiType" type="spiralType" use="required"/>
        //<xs:attribute name="chord" type="xs:double"/>
        //<xs:attribute name="constant" type="xs:double"/>
        //<xs:attribute name="desc" type="xs:string"/>
        //<xs:attribute name="dirEnd" type="direction"/>
        //<xs:attribute name="dirStart" type="direction"/>
        //<xs:attribute name="name" type="xs:string"/>
        //<xs:attribute name="theta" type="angle"/>
        //<xs:attribute name="totalY" type="xs:double"/>
        //<xs:attribute name="totalX" type="xs:double"/>
        //<xs:attribute name="staStart" type="xs:double"/>
        //<xs:attribute name="state" type="stateType"/>
        //<xs:attribute name="tanLong" type="xs:double"/>
        //<xs:attribute name="tanShort" type="xs:double"/>
        //<xs:attribute name="oID" type="xs:string"/>
        //</xs:complexType>
        //</xs:element>
        #endregion
        
        #region Required
        /// <summary>
        /// 
        /// </summary>
        double Length { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double RadiusStart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double RadiusEnd { get; set; }
        /// <summary>
        /// The rotation attribute "rot" defines whether the arc travels clockwise or counter-clockwise from the Start to End point.
        /// </summary>
        ClockWise Rotation { get; set; }
        /// <summary>
        /// 
        /// </summary>
        SpiralType SpiralType { get; set; }
        #endregion

        #region Optional
        /// <summary>
        /// 
        /// </summary>
        double Chord { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double Constant { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Description { get; set; }
        /// <summary>
        /// Represents a normalized angular value that indicates a horizontal direction, expressed in the specified Direction units. Assume 0 degrees = north
        /// </summary>
        double DirectionEnd { get; set; }
        /// <summary>
        /// Represents a normalized angular value that indicates a horizontal direction, expressed in the specified Direction units. Assume 0 degrees = north
        /// </summary>
        double DirectionStart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string Name { get; set; }
        double Theta { get; set; }
        double TotalX { get; set; }
        double TotalY { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double StationStart { get; set; }
        /// <summary>
        /// 
        /// </summary>
        double StationEnd { get; }
        /// <summary>
        /// 
        /// </summary>
        StateType? State { get; set; }
        double TanLong { get; set; }
        double TanShort { get; set; }
        /// <summary>
        /// Official ID
        /// </summary>
        string OId { get; set; }
        #endregion

        #region No Idea => To be verified
        /// <summary>
        /// Represents a 2D or 3D Starting Point
        /// </summary>
        IPointTypeElement Start { get; set; }
        /// <summary>
        /// Represents a 2D or 3D Ending Point
        /// </summary>
        IPointTypeElement End { get; set; }
        /// <summary>
        /// Represents a 2D or 3D Point Of Intersection
        /// </summary>
        IPointTypeElement Pi { get; set; }
        /// <summary>
        /// Used to include additional information that is not explicitly defined by the LandXML schema
        /// Feature may contain one or more Property, DocFileRef or nested Feature Elements. 
        /// Note: to allow any valid content, the explicit definitions for Property, DocFileRef and Feature habe been commented out, but are still excpected in common use.
        /// </summary>
        IList<IFeatureElement> Features { get; set; }
        #endregion
    }
}
