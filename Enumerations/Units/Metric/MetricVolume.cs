﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricVolume
    {
        // ReSharper disable InconsistentNaming
        cubicMeter,
        liter,
        hectareMeter
        // ReSharper restore InconsistentNaming
    }
}
