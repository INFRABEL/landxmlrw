﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;

namespace LANDXMLRW.Model.Alignment
{
    public class Cant : ICant
    {
        private IList<ICantStation> _cantStations = new List<ICantStation>();
        public IList<ICantStation> CantStations
        {
            get { return _cantStations; }
            set
            {
                _cantStations = value;
            }
        }

        private IList<ISpeedStation> _speedStations = new List<ISpeedStation>();
        public IList<ISpeedStation> SpeedStations
        {
            get { return _speedStations; }
            set
            {
                _speedStations = value;
            }
        }

        public string Name { get; set; }

        public double Gauge { get; set; }

        public string Description { get; set; }

        public StateType State { get; set; }

        public double EquilibriumConstant { get; set; }

        public double AppliedCantConstant { get; set; }

        public string RotationPoint { get; set; }

        private IList<IFeatureElement> _features = new List<IFeatureElement>();
        public IList<IFeatureElement> Features
        {
            get { return _features; }
            set
            {
                _features = value;
            }
        }
    }
}
