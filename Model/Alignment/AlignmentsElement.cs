﻿using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using System.Collections.Generic;

namespace LANDXMLRW.Model.Alignment
{
    public class AlignmentsElement : IAlignmentsElement
    {
        /// <summary>
        /// This class holds a collection of horizontal Alignments, and a collection of features. The class has a name, description, and a state enumerator.
        /// </summary>
        public IList<IAlignmentElement> Alignments { get; set; }

        /// <summary>
        /// Used to include additional information that is not explicitly defined by the LandXML schema,
        /// Feature may contain one or more Property, DocFileRef or nested Feature elements.
        /// NOTE: to allow any valid content, the explicit definitions for Property, DocFileRef
        /// and Feature have been commented out, but are still expected in common use.
        /// </summary>
        public IList<IFeatureElement> Features { get; set; }

        /// <summary>
        /// Name of the alignment
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of the alignment
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// abandoned - destroyed - existing - proposed
        /// </summary>
        public StateType State { get; set; }

        /// <summary>
        /// Creates a new instance of the AlignmentsElement object.
        /// </summary>
        public AlignmentsElement()
        {
            Features = new List<IFeatureElement>();
            Alignments = new List<IAlignmentElement>();
        }
    }
}