﻿namespace LANDXMLRW.Enumerations.Units.Metric
{
    public enum MetricLinear
    {
        // ReSharper disable InconsistentNaming
        millimeter,
        centimeter,
        meter,
        kilometer
        // ReSharper restore InconsistentNaming
    }
}
