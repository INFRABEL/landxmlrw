﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.GoordGeom
{
  public class LineElement : ILineElement
  {
    public IPointTypeElement Start { get; set; }

    public IPointTypeElement End { get; set; }

    public IList<IFeatureElement> Features { get; set; }

    public string Description { get; set; }

    public double Direction { get; set; }

    public double Length { get; set; }

    public string Name { get; set; }

    public double StationStart { get; set; }
        
    public double StationEnd{ get { return StationStart + Length; } }

    public StateType? State { get; set; }

    public string OId { get; set; }

    public string Note { get; set; }
  }
}
