﻿namespace LANDXMLRW.Enumerations.Units.Imperial
{
    public enum ImperialDiameter
    {
        // ReSharper disable InconsistentNaming
        foot,
        USSurveyFoot,
        inch
        // ReSharper restore InconsistentNaming
    }
}
