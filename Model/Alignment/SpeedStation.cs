﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LANDXMLRW.Model.Alignment
{
    public class SpeedStation : ISpeedStation
    {
        public double Station { get; set; }

        public double Speed { get; set; }
    }
}
