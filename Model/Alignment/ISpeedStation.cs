﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LANDXMLRW.Model.Alignment
{
    /// <summary>
    /// A cant speed-only station. The “station” is a required double that is internal station value.
    /// The “speed” is an optional double that is the design speed.  This value is in kmph or mph depending upon the units.
    /// Optional??? XSD describes REQUIRED!!! :)
    /// </summary>
    public interface ISpeedStation
    {
        #region XML Documentation 1.2
        //<xs:element name="SpeedStation">
        //  <xs:annotation>
        //    <xs:documentation>A cant speed-only station.
        //            The “station” is a required double that is internal station value.
        //The “speed” is an optional double that is the design speed.  This value is in kmph or mph depending upon the units.
        //</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:attribute name="station" type="xs:double" use="required"/>
        //    <xs:attribute name="speed" type="xs:double" use="required"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        double Station { get; set; }
        double Speed { get; set; }
        #endregion
    }
}
