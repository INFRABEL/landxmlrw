﻿namespace LANDXMLRW.Enumerations
{
    public enum SpiralType
    {
        // ReSharper disable InconsistentNaming
        biquadratic,
        bloss,
        clothoid,
        cosine,
        cubic,
        sinusoid,
        revBiquadratic,
        revBloss,
        revCosine,
        revSinusoid,
        sineHalfWave,
        biquadraticParabola,
        cubicParabola,
        japaneseCubic,
        radioid,
        weinerBogen
        // ReSharper restore InconsistentNaming
    }
}