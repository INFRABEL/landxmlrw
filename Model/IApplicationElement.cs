﻿using System;

namespace LANDXMLRW.Model
{
    /// <summary>
    /// Optional element to identify the software that was used to create the file.
    /// </summary>
    public interface IApplicationElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Application">
        //  <xs:annotation>
        //    <xs:documentation>Optional element to identify the software that was used to create the file.</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType mixed="true">
        //    <xs:sequence>
        //      <xs:choice>
        //        <xs:element ref="Author" minOccurs="0" maxOccurs="unbounded"/>
        //        <xs:any namespace="##other" processContents="skip" minOccurs="0"/>
        //      </xs:choice>
        //    </xs:sequence>
        //    <xs:attribute name="name" type="xs:string" use="required"/>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="manufacturer" type="xs:string"/>
        //    <xs:attribute name="version" type="xs:string"/>
        //    <xs:attribute name="manufacturerURL" type="xs:string"/>
        //    <xs:attribute name="timeStamp" type="xs:dateTime" use="optional"/>
        //    <xs:anyAttribute/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Required
        string Name { get; set; }
        #endregion

        #region Optional
        IAuthorElement Author { get; set; }
        string Description { get; set; }
        string Manufacturer { get; set; }
        string Version { get; set; }
        string ManufacturerUrl { get; set; }
        DateTime TimeStamp { get; set; }
        #endregion
    }
}
