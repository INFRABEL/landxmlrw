﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;

namespace LANDXMLRW.Model.Profile
{
    public class ProfSurf : IProfSurf
    {
        private IList<IPointElement> _pointElements = new List<IPointElement>(); 
        public IList<IPointElement> Point2DList
        {
            get { return _pointElements; }
            set
            {
                _pointElements = value;
            }
        }

        private IList<IFeatureElement> _features = new List<IFeatureElement>(); 
        public IList<IFeatureElement> Features
        {
            get
            {
                return _features;
            }
            set { _features = value; }
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public StateType? State { get; set; }
    }
}
