﻿using System.Collections.Generic;
using LANDXMLRW.Enumerations;
using LANDXMLRW.Model.Feature;
using LANDXMLRW.Model.Geometry;
using LANDXMLRW.Model.GoordGeom;
using LANDXMLRW.Model.Profile;

namespace LANDXMLRW.Model.Alignment
{
    /// <summary>
    /// Geometric horizontal alignment, PGL or chain typically representing a road design center line
    /// </summary>
    public interface IAlignmentElement
    {
        #region XSD Documentation 1.2
        //<xs:element name="Alignment">
        //  <xs:annotation>
        //    <xs:documentation>geometric horizontal alignment, PGL or chain typically representing a road design center line</xs:documentation>
        //  </xs:annotation>
        //  <xs:complexType>
        //    <xs:choice maxOccurs="unbounded">
        //      <xs:choice>
        //        <xs:element ref="Start" minOccurs="0"/>
        //        <xs:element ref="CoordGeom"/>
        //        <xs:element ref="AlignPIs" minOccurs="0"/>
        //        <xs:element ref="Cant" minOccurs="0"/>
        //      </xs:choice>
        //      <xs:element ref="StaEquation" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Profile" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="CrossSects" minOccurs="0"/>
        //      <xs:element ref="Superelevation" minOccurs="0" maxOccurs="unbounded"/>
        //      <xs:element ref="Feature" minOccurs="0" maxOccurs="unbounded"/>
        //    </xs:choice>
        //    <xs:attribute name="name" type="xs:string" use="required"/>
        //    <xs:attribute name="length" type="xs:double" use="required"/>
        //    <xs:attribute name="staStart" type="xs:double" use="required"/>
        //    <xs:attribute name="desc" type="xs:string"/>
        //    <xs:attribute name="oID" type="xs:string"/>
        //    <xs:attribute name="state" type="stateType"/>
        //  </xs:complexType>
        //</xs:element>
        #endregion

        #region Choice
        IPointTypeElement Start { get; set; }
        IList<IGeoElement> CoordGeom { get; set; }
        IList<IPointTypeElement> PIs { get; set; }
        ICant Cant { get; set; }
        #endregion

        #region Required
        string Name { get; set; }
        double Length { get; set; }
        double StaStart { get; set; }
        #endregion

        #region Optional
        string Description { get; set; }
        string OId { get; set; }
        StateType? State { get; set; }
        IList<IFeatureElement> Features { get; set; }
        IProfile Profile { get; set; }
        #endregion
    }
}
